$(document).ready(function(){
    
    // Add Class
    $('.edit').click(function(){
        $(this).addClass('editMode');
    
    });

    // Save data
    $(".edit").focusout(function(){
        $(this).removeClass("editMode");
 
        var id = this.id;
        /*var split_id = id.split("_");
        var table_name = split_id[0];
        var field_name = split_id[1];
        var edit_id = split_id[2];*/

        var value = $(this).text();
     
        $.ajax({
            url: '../controller/update.php',
            type: 'post',
            //data: { table:table_name, field:field_name, value:value, id:id },
            data: {allData:id, value:value },
            success:function(response){
                console.log('Save successfully');               
            }
        });
                
    });

});
