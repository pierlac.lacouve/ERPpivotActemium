/**
 * Created by LARBI on 14/05/2018.
 */
alert("Le login ou le mot de passe est incorrect.");
function confirmation(texte,page){
    if(confirm(texte))
        document.location.href = page;
}
$(document).ready(function () {
    $('.forgot-pass').click(function(event) {
        $(".pr-wrap").toggleClass("show-pass-reset");
    });

    $('.pass-reset-submit').click(function(event) {
        $(".pr-wrap").removeClass("show-pass-reset");
    });
});