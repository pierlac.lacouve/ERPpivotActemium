<?php include "C:/wamp64/www/traitement-pivot/controller/auto-import.php"; ?>
<header  class="header-desktop3 d-none d-lg-block fixed-top">
    <div class="section__content section__content--p35">
        <div class="header3-wrap">
            <div class="header__logo" style="width: 12.5%">
                <a href="accueil.php">
                    <img src="../assets/images/facebook-cropped-alpha.png" alt="page d'acceuil" />
                </a>
            </div>
            <div class="header__navbar">
                <ul class="list-unstyled">
                    <?php
                    foreach ($_SESSION['authorizations']->getHeaderPages() as $page)
                    {
                        $isSelected="";
                        if($_SESSION['authorizations']->isSelectedHeaderPage($_SERVER['PHP_SELF'],$page,$pagesPaths))
                        {
                            $isSelected="selected";
                        }
                        print ("<li class=\"has-sub\">
                                    <a class =\"$isSelected\" href=\"$pagesPaths[$page]\">
                                        <i class=\"fas fa-$pageLogo[$page]\"></i>$page
                                        <span class=\"bot-line\"/>
                                    </a>
                                </li>");
                    }
                    ?>
                </ul>
            </div>
            <div class="header__logo dropdown" style="font-size: x-large" onmouseover="display('dropdown-content')">
                <a href="my-account.php">
                    <button class="dropbtn">
                        <span style="color:#004187" ><?php echo $_SESSION['firstname']." ".$_SESSION['lastname'];?></span>
                    </button>
                </a>
                <div id="dropdown-content" class="dropdown-content" style="display: none;" onmouseleave="smoothHide('dropdown-content')">
                    <a href="change-password.php">
                        <i class="fas fa-cog"></i>
                        Changer mon mot de passe
                    </a>
                    <a href="../controller/logout.php" class="logout">
                            <i class="fas fa-power-off"></i>
                            Se déconnecter
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<body>
<a title="Help" class="help-button" href="../assets/users-manual.pdf">
    <i class="fas fa-question-circle"></i>
</a>
</body>







