<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";

$result=$db->export_pivot();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
	<?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>Export Codex</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <!-- PAGE CONTENT-->
    <div class="container spacer2">
        <div class="row spacer2 ">
            <form method="post" action="../controller/export-codex.php">
                <div class="col-lg-12 ">
                    <!-- Advanced Tables -->
                    <div class="login-panel panel default-panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Codex</h3>
                        </div>

                        <div class="panel-body">
                            <div class="row topnav">
                                <div class="search-container" style="margin: 15px">
                                    <i class="fa fa-search col-md-1"> </i>
                                    <input class="col-md-9 col-md-offset-1" type="text" id="myInput" onkeyup="searchIn()" placeholder="Rechercher..." title="Taper pour rechercher">
                                </div>
                            </div>
                            <div class="tableFixHead" style="height: 450px">
                                <table>
                                    <thead style="background-color:#0d70b7">
                                        <tr style="color:black ">
                                            <th> </th>
                                            <th>Affaire</th>
                                            <th>S/Ens ou poste</th>
                                            <th>Numéro Plan</th>
                                            <th>Ind Plan</th>
                                            <th>Designation</th>
                                            <th>Qté unit</th>
                                            <th>Qté totale</th>
                                            <th>Type</th>
                                            <th style="width: 150px">Référence</th>
                                            <th style="width: 150px">Fabricant</th>
                                            <th>Statut</th>
                                            <th>Observation</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableContent">
                                        <?php
                                        foreach ($result as $row) {
                                            $reqBis = $db->select(array("id_affaire", "id_parent", "id_self"), array("hierarchies"),
                                                    array("id_affaire='" . $row["Affaire"] . "'", "id_parent='" . $row["Id"] . "'"), 0);
                                            $dataArr = array("'" . $row["Affaire"] ."-". $row["IdParent"] ."-" . $row["Id"] . "'");
                                            while ($dataBis = $reqBis->fetch()) {
                                                $dataArr[] = "'" . $dataBis["id_affaire"] ."-". $dataBis["id_parent"] ."-". $dataBis["id_self"] . "'";
                                            }
                                            print("<tr style='height:60px;'>");
                                            $dataArrImp = implode(',', $dataArr);
                                            print("<td><input type=\"checkbox\" id=" . $dataArr[0] . "
                                                              name=" . $dataArr[0] . " 
                                                              onchange=\"selectSons(" . $dataArrImp . ")\"></td>");

                                            printTd($row["Affaire"]); // Affaire
                                            printTd($row["Parent"]); // Parent
                                            printTd($row["NumPlan"]); // Numéro de Plan
                                            printTd($row["Indice"]); // Indice
                                            printTd($row["Designation"]); // Désignation
                                            printTd($row["QteU"]); // Quantité unitaire
                                            printTd($row["QteT"]); // Quantité totale
                                            printTd($row["EltType"]); // Type d'élément

                                            $resultNum = intval($db->select(array("count(*)"), array("fabricants_elements fe"), array("fe.id_element=" . $row["Id"]))[0]);

                                            $req2 = $db->select(array("fe.reference", "f.nom","f.id"), array("fabricants_elements fe", "fabricants f"),
                                                    array("f.id=fe.id_fabricant", "fe.id_element=" . $row["Id"]), 0);
                                            print("<td colspan='2'><div id='" . $row["Affaire"] ."-". $row["IdParent"] ."-". $row["Id"] . "div'>");
                                            for ($i=0;$i<$resultNum;++$i){
                                                $data= $req2->fetch();
                                                print("<div>");
                                                print("<input class='radio-buttons-" . $row["Affaire"] ."-". $row["IdParent"] ."-". $row["Id"] . "' style='display:none' type='radio' name='".$row["Id"]."' id='".$data[2]."' value='".$data[2]."'");
                                                if($i==0)
                                                    print(" checked");
                                                print(">");
                                                print("<label for='".$data[2]."'>".$data[1] . " - " . $data[0]."</label>");
                                                print("</div>");
																						}
                                            print("</div></td>");
                                            printTd($row["Statut"]); // Observation
                                            printTd($row["Obs"]); // Observation
                                            print("</tr>");
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <input name="Import-codex" type="submit" class="col-md-offset-5 col-md-2 au-btn au-btn-icon au-btn--green au-btn--small" value="Download">
            </form>
        </div>
    </div>

</div>
<script>
    function selectSons(){
        var i;

        if(document.getElementById(arguments[0]).checked)
            for(i=0;i<document.getElementsByClassName("radio-buttons-".concat(arguments[0])).length;++i){
                document.getElementsByClassName("radio-buttons-".concat(arguments[0])).item(i).style.display="inherit";//display
            }
        else
            for(i=0;i<document.getElementsByClassName("radio-buttons-".concat(arguments[0])).length;++i){
                document.getElementsByClassName("radio-buttons-".concat(arguments[0])).item(i).style.display="none";//hide
            }

        for(i=1;i<arguments.length;i++){
            document.getElementById(arguments[i]).checked=document.getElementById(arguments[0]).checked;
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", false, true);
            document.getElementById(arguments[i]).dispatchEvent(evt);

        }
    }
</script>
<?php include ("../controller/scripts.html");?>
</body>
</html>
<!-- end document-->