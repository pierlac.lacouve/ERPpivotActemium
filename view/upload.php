<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
    <?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>Importer</title>
</head>
<body>
<?php include("header.php") ?>
<!--  wrapper -->
<div class="animsition">
   <!--  page-wrapper -->
    <div class="page-wrapper">
        <div class="page-content--bgf7 spacer">
            <?php
            if (isset($_SESSION['erreur']) || isset($_SESSION['OK'])) {
                print("<div style=\"color: ");
                if (isset($_SESSION['erreur'])) {
                    print("red;height:100px;overflow:auto;\">");
                    print_arr($_SESSION['erreur']);
                    unset($_SESSION['erreur']);
                } elseif (isset($_SESSION['OK'])) {
                    print("green;background:#ffff00;text-align: center;font-size:xx-large\">");
                    echo $_SESSION['OK'];
                    unset($_SESSION['OK']);
                }
                print("</div>");
            }
            $request=$db->select(array("*"),array("import_configuration"),array(1),0);
            print("<div class=\"row\">");
            for($i=1;$data=$request->fetch();++$i){
                print("<div class=\"container col-md-3\"><div class=\"panel panel-default spacer1\" style='height: available'>");
                    print("<div class=\"panel-heading\" style=\"background-color: ".$data["color"]."\"><b>".$data["nom"]."</b></div>");
                print("<div class=\"panel-body\">");
                    print("<form method=\"post\" action=\"../controller/import-general.php\" enctype=\"multipart/form-data\">");
                        print("<div class=\"form-group\" style='height: 8rem;'>");
                            print("<img class='zmdi-crop-square' style=\"width: 15%;margin: 1rem\" src=\"".$data["path_icon"]."\">");
                            print("<label class=\"control-label\">Numéro d'affaire :</label>");
                            print("<select type=\"text\" class=\"col-md-12\" name=\"affaire\" required>");
                                print("<option selected=\"selected\"></option>");
                                $db->getAffaires(false,true);
                            print("</select>");
                        print("</div><div class=\"form-group\" style='margin-top: 4rem'>");
                            print("<label class='control-label'>Fichier");
                            if($data["import_mode"]==0)
                                print("(s)");
                            print("</label>");
                            print("<input type=\"file\" name=\"file");
							if($data["import_mode"]==0)
								print("[]\" multiple=\"");
							print("\" class=\"form-control\"/>");
                        print("</div><div class=\"form-group\" style='margin-top: 2rem'>");
                        print("<input ");
                            if ($_SESSION["authorizations"]->getLevel()==0){
                                print("type=\"button\"");
                            }else {
                                print("type=\"submit\"");
                            }
                        print(" name=\"".$data["id"]."\" class=\"au-btn au-btn-icon au-btn--green au-btn--small col-md-12\" value=\"Upload\"/>");
                    print("</div></form>");
                print("</div></div></div>");

                if($i%4==0)
                    print("</div><div class=\"row\">");
            }
            print("</div>");
            ?>
        </div>
    </div>
</div>
<?php include ("../controller/scripts.html");?>
</body>
</html>