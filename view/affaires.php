<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
if($_SESSION["authorizations"]->getLevel()==0){
	$unavailable=" style='display:none'";
}
else{
    $unavailable="";
}
if(isset($_POST["modif-aff"]) && $_POST["modif-aff"]!="Cancel"){
    $db->update("affaires",array('login_chef_projet'),array($_POST["login_chef_projet"]),array("id='".$_POST["modif-aff"]."'"));
	unset($_POST);
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
    <?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>Suivi affaires</title>
</head>
<body class="animsition">
    <!-- HEADER DESKTOP-->
    <?php include("header.php");?>
    <!-- END HEADER DESKTOP -->
    <div class="page-wrapper">
        <!-- Formulaire de creation d'affaire-->
        <div class="container spacer2">
            <div class="row spacer2 ">
                <div class="col-md-10 col-md-offset-1">


                    <?php
                    if(isset($_SESSION["affaire-send"])){
                        $_POST["affaire-send"]=$_SESSION["affaire-send"];
                        unset($_SESSION["affaire-send"]);
                    }

                    if(isset($_POST["affaire-send"])) {
    print("<div class=\"login-panel panel default-panel\">
    <div class=\"panel-heading\">
        <!-- CHART-->
        <h3 class=\"panel-title\">Chef de projet</h3>
    </div>
    <div class=\"panel-body\">
        <form method='post' action='affaires.php'>
        <div class='form-group col-md-6 col-md-offset-3'>
        <label for='0'> Chef de projet pour l'affaire : " . $_POST["affaire-send"] . "</label>
        <select id='0' name=\"login_chef_projet\" class=\"form-control form-control-lg\" required>");

    $ps = $db->select(array("*"), array("users"), array("authorizations>0"), 0);
    print("<option selected value=\"NULL\">-</option>");
    while ($usersData = $ps->fetch()) {
			$select = "";
			if ($usersData["login"] == $_POST["Login"])
				$select = "selected";
			print("<option value='" . $usersData["login"] . "'" . $select . ">" . $usersData["firstname"] . " " . $usersData["lastname"] . "</option>");
		}
    print("</select>
        </div>
        <div class='form-group col-md-4 col-md-offset-2'>
        <button class=\"form-group col-md-12 au-btn au-btn-icon au-btn--green au-btn--small\"  type=\"submit\" name=\"modif-aff\" value='".$_POST["affaire-send"]."'>
    <i class=\"zmdi \"></i>Envoyer</button></div>
        <div class='form-group col-md-4'>
        <button class=\"form-group col-md-12 au-btn au-btn-icon au-btn--blue au-btn--small\"  type=\"submit\" name=\"modif-aff\" value='Cancel'>
    <i class=\"zmdi \"></i>Annuler</button></div>
</form>
    </div>
</div>");
}
                    ?>

                    <div class="login-panel panel default-panel">
                        <div class="panel-heading">
                            <!-- CHART-->
                            <h3 class="panel-title">Affaires</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row topnav">
                                <div class="search-container" style="margin: 15px">
                                    <i class="fa fa-search col-md-1"> </i>
                                    <input class="col-md-9 col-md-offset-1" type="text" id="myInput" onkeyup="searchIn()" placeholder="Rechercher..." title="Taper pour rechercher">
                                </div>
                            </div>

                            <div class="tableFixHead">
                                <table>
                                    <thead>
                                    <tr>
                                        <th style="width=15%">No.</th>
                                        <th  style="width=30%">Libellé</th>
                                        <th style="width=30%">Client</th>
                                        <th colspan="2" style="width=25%">Chef de projet</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tableContent">
                                    <?php
                                    $res=$db->select(array("a.id IdAff","c.nom Client","a.designation Des","u.firstname FName","u.lastname LName, u.login Login"),
                                            array("affaires a LEFT JOIN users u ON u.login=a.login_chef_projet","clients c"),
                                            array("a.id_client=c.id"),0,"a.id");
                                    foreach ($res as $row) {
                                        $res2=$db->select(array("id_affaire"),array("hierarchies"),array("id_affaire='".$row["IdAff"]."'"));
                                        $color="";
                                        if(isset($res2) && isset($res2[0]))
                                            $color=" style=\"color:green\"";
                                        $chefZone="<td>".$row["FName"] . " " . $row["LName"]."</td><td><form method=\"post\" action=\"affaires.php\">
                                        <button".$unavailable." class=\"form-group au-btn--small\"  type=\"submit\" name='affaire-send' value=\"".$row["IdAff"]."\">
                                            <div id='LoadIcon' class=\"fas fa-cog\" onmouseover='spin()' onmouseleave='unspin()'></div>
                                        </button>
                                        <input type=\"hidden\" value=\"".$row["Login"]."\" name=\"Login\">
                                        </form></td>";

                                        if($row["FName"]=="")
                                            $chefZone="<td colspan='2'><form method=\"post\" action=\"affaires.php\">
                                                        <button".$unavailable." class=\"form-group au-btn au-btn--green au-btn--small\"  type=\"submit\" name='affaire-send'   value=\"".$row["IdAff"]."\">
                                                            <i class=\"zmdi \"></i>Selectionner
                                                        </button>
                                                        </form></td>";
                                        print "<tr".$color."><td>" . $row["IdAff"] . "</td><td>" . $row["Des"] . "</td><td>" . $row["Client"] . "</td>" . $chefZone . "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>


                            <form method="post" action="../controller/reload-affaire.php">
                                <button style="margin: auto" class="col-md-12 login-panel panel default-panel" onmouseover="spin()" onmouseleave="unspin()" type="submit" name="loadAff">
                                    <div class="panel-body" style="text-align: center">
                                        <i class="fas fa-sync" id="LoadIcon"></i>
                                        Actualiser
                                    </div>
                                </button>
                            </form>
                        </div>

                    <?php
                    if (isset($_SESSION['erreur']) || isset($_SESSION['OK'])) {
                        print("<div style=\"color: ");
                        if (isset($_SESSION['erreur'])) {
                            print("red;height:100px;overflow:auto;\">");
                            print_arr($_SESSION['erreur']);
                            unset($_SESSION['erreur']);
                        } elseif (isset($_SESSION['OK'])) {
                            print("green;text-align: center\">");
                            echo $_SESSION['OK'];
                            unset($_SESSION['OK']);
                        }
                        print("</div>");
                    }
                    ?>
                        
                    </div>




                    <button <?php print($unavailable)?> id="buttonShow" class="col-md-12 login-panel panel default-panel" onclick="showAdvanced()">
                        <div class="panel-body" style="text-align: center">
                            <i class="fas fa-plus-circle"></i>
                            Paramètres avancés
                        </div>
                    </button>
                    <div id="advanced" style="display: none;">
                        <div class="login-panel panel default-panel">
                            <div class="panel-heading">
                                <!-- CHART-->
                                <h3 class="panel-title">Saisir une nouvelle affaire</h3>
                            </div>
                            <div class="panel-body">
                                <form method="post" action="../controller/insert-affaire.php" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="0">Numéro d'affaire *</label>
                                            <input type="text" name="id_affaire" class="form-control" id="0" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="1">Client *</label>
                                            <select name="client" class="form-control form-control-lg" id="1" required>
                                                                                        <?php
                                                                                        $ps=$db->select(array("*"),array("clients"),array(),0);
                                                                                        while ($usersData=$ps->fetch())
                                                                                            print("<option value='".$usersData["id"]."'>".$usersData["nom"]."</option>");
                                                                                        ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="2">Chef de projet *</label>
                                            <select name="login_chef_projet" class="form-control form-control-lg" id="2" required>
                                                                                        <?php
                                                                                        $ps=$db->select(array("*"),array("users"),array(),0);
                                                                                        while ($usersData=$ps->fetch())
                                                                                            print("<option value='".$usersData["login"]."'>".$usersData["firstname"]." ".$usersData["lastname"]."</option>");
                                                                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="3">Montant</label>
                                            <input type="number" name="montant" class="form-control" id="3">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="4">Date de création *</label>
                                            <input type="date" name="date" class="form-control" id="4">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="5">Etat actuel</label>
                                            <input type="text" name="state" class="form-control" id="5">
                                        </div>
                                    </div>
                                    <button class="form-group col-md-8 col-md-offset-2 au-btn au-btn-icon au-btn--green au-btn--small"  type="submit" name="enregistrer">
                                        <i class="zmdi "></i>Envoyer</button>
                                </form>
                            </div>
                            <div class="panel-heading">
                                <!-- CHART-->
                                <h3 class="panel-title">Nouveau client</h3>
                            </div>
                            <div class="panel-body">
                                <form method="post" action="../controller/insert-client.php" enctype="multipart/form-data">
                                    <div class="form-group col-md-6">
                                        <input type="text" name="nom" class="form-control" id="0" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <button class="form-group col-md-12 au-btn au-btn-icon au-btn--green au-btn--small"  type="submit" name="enregistrer">
                                            <i class="zmdi "></i>Ajouter
                                        </button>
                                    </div>

                                </form>
                                <button style="margin: auto" class="col-md-12 login-panel panel default-panel" onclick="hideAdvanced()">
                                    <div class="panel-body" style="text-align: center">
                                        <i class="fas fa-minus-circle"></i>
                                        Cacher
                                    </div>
                                </button>
                            </div>
                        </div>

                    </div>
                    <!-- END CHART-->
                </div>
            </div>
        </div>

    </div>

    <?php include ("../controller/scripts.html");?>
    <script>
    function showAdvanced() {
        document.getElementById("advanced").style.display="block";
        document.getElementById("buttonShow").style.display="none";
    }
    </script>
    <script>
    function hideAdvanced() {
        document.getElementById("advanced").style.display="none";
        document.getElementById("buttonShow").style.display="block";
    }
    </script>
    <script>
    function spin() {
        document.getElementById("LoadIcon").classList.add("fa-spin");
    }
    </script>
    <script>
    function unspin() {
        document.getElementById("LoadIcon").classList.remove("fa-spin");
    }
    </script>
</body>
</html>
<!-- end document-->