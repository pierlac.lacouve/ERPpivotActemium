<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include("../controller/head.html") ?>
    <title>Mon compte Actemium</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <div class="page-content--bgf7 ">
        <!-- STATISTIC-->
        <section class="statistic statistic2 spacer">
            <div class="container">
                <div class="col-md-4 col-md-offset-1">
                    <a class="col-md-12" href="change-password.php">
                        <div class="statistic__item statistic__item--blue">
                            <h2 class="number" >Mon mot de passe</h2>
                            <span class="desc">Modifier le mot de passe actuel</span>
                            <div class="icon">
                                <i class="zmdi zmdi-border-color">

                                </i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-md-offset-2">
                    <a class="col-md-12" href="../controller/logout.php">
                        <div class="statistic__item statistic__item--red">
                            <h2 class="number" >Se déconnecter</h2>
                            <span class="desc">Fermer la session</span>
                            <div class="icon">
                                <i class="zmdi zmdi-close-circle-o">

                                </i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
    </div>
</div>

<?php include("../controller/scripts.html") ?>

</body>

</html>
<!-- end document-->
