<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";

$ps=$db->select(array("*"),array("affaires"),array(),0,"montant DESC",5);
$ps1=$db->select(array("*"),array("affaires a","clients c"),array("c.id = a.id"),0);
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <!--HEAD IMPORT-->
        <?php include("../controller/head.html");?>
        <!-- Title Page-->
        <title>Accueil</title>
    </head>
    <body class="animsition">
        <!-- HEADER DESKTOP-->
        <?php include("header.php") ?>
        <!-- END HEADER DESKTOP -->
        <div class="page-wrapper">
            <!-- PAGE CONTENT-->
            <div class="page-content--bgf7 ">
                <!-- STATISTIC CHART-->
                <section class="statistic-chart">
                    <div class="container">
                        <div class="row" style="margin-top: 15rem">
                            <div class="col-md-6 col-md-offset-3">
                                <!-- TOP CAMPAIGN-->
                                <div class="top-campaign">
                                    <h3 class="title-3 m-b-30">Extraire un fichier Pivot au format Excel</h3>
                                    <div>
                                        <form method="post" action="../controller/export-pivot.php">
                                            <div class="form-group">
                                                <div class="form-row">
                                                    <select type="text" class="col-md-12" name="affaire" required>
                                                        <option selected="selected"></option>
                                                        <?php $db->getAffaires(true)?>
                                                    </select>
                                                </div>
                                                <div class="form-row">
                                                    <input
                                                        <?php
                                                        if ($_SESSION["authorizations"]->getLevel()==0){?>
                                                            type="button"
                                                        <?php }else {?>
                                                            type="submit"
                                                        <?php }?>
                                                            class="au-btn au-btn-icon au-btn--green au-btn--small" name="Import-pivot" value="Télécharger"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- END TOP CAMPAIGN-->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END STATISTIC CHART-->
            </div>
        </div>
    <?php include ("../controller/scripts.html");?>
    </body>
</html>
<!-- end document-->