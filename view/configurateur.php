<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";

if(isset($_POST["delete"]))
	$db->delete("import_configuration",array("id=".$_POST["delete"]));

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
    <?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>Gestion d'imports</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <!-- Formulaire de creation d'affaire-->
    <div class="container spacer2">
        <div class="row spacer2 ">
            <div class="col-md-10 col-md-offset-1">
							<?php
							if (isset($_SESSION['erreur']) || isset($_SESSION['OK'])) {
								print("<div style=\"color: ");
								if (isset($_SESSION['erreur'])) {
									print("red;height:100px;overflow:auto;\">");
									print_arr($_SESSION['erreur']);
									unset($_SESSION['erreur']);
								} elseif (isset($_SESSION['OK'])) {
									print("green;text-align: center\">");
									echo $_SESSION['OK'];
									unset($_SESSION['OK']);
								}
								print("</div>");
							}
							?>
              <?php
              if(isset($_SESSION["restart-import"])){
                  $_POST=$_SESSION["restart-import"];
                  unset($_SESSION["restart-import"]);
              }
              if(isset($_POST["edit-import"])) {
                    print("<div class=\"login-panel panel default-panel\">
                    <div class=\"panel-heading\"><h3 class=\"panel-title\">");
                    if ($_POST["edit-import"] == "new")
                        print("Ajouter un mode d'import CAO");
                    else {
                        $dataImport = $db->select(array("*"), array("import_configuration"),array("id=".$_POST["edit-import"]));
                        print("Modifier l'import CAO n°" . $_POST["edit-import"]);
                    }

								print("</h3>
                    </div>
                    <div class=\"panel-body\">
                            <form method='post' enctype=\"multipart/form-data\" action='../controller/config-send.php'>
                                <div class='form-group col-md-12'>");

								/*Form zone*/
								print("<div class='row'><h4>Caractéristiques</h4></div>");

								print("<div class='form-group'>");

								print("<div class='row'>");
								print("<div class='col-md-6'>");
								print("<label class='col-md-4' for='0'>Nom de configuration</label>");
								print('<input class="col-md-7" style="background: #dceeff" id="0" type ="text" name="nom"');
								if(isset($dataImport["nom"]))
								    print(' value="'.$dataImport["nom"].'"');
                                else
                                    print(" placeholder=\"Nom de la configuration d'import\"");
								print('>');
								print("</div>");

								print("<div class='col-md-6'>");
								print("<label class='col-md-4' for='4'>Charger une icône</label>");
								print('<input class="col-md-7" id="4" type ="file" name="file">');
								print("</div>");
								print("</div>");

								print("<div class='row'>");
								print("<div class='col-md-4'>");
								print("<label class='col-md-11' for='1' title=\"A cocher en cas d'import selon une structure hiérarchisée (en niveaux)\">Importer en mode fichier unique</label>");
								if(!isset($dataImport["import_mode"]))
									$dataImport["import_mode"]="0";
								print('<input class=\'col-md-1\' id="1" type ="checkbox" name="import_mode" ');
								if($dataImport["import_mode"]==1)
								    print("checked ");
                                print('onchange="levelShowHide()">');
								print("</div>");

								print("<div class='col-md-4' >");
								print("<label class='col-md-10' for='2'>Couleur d'entête</label>");
								if(!isset($dataImport["color"]))
									    $dataImport["color"]="#FFFFFF";
								print('<input class=\'col-md-2\' id="2" type ="color" name="color" style=\'background: #FFFFFF\' value="'.$dataImport["color"].'"">');
								print("</div>");

								print("<div class='col-md-4'>");
								print("<label class='col-md-9' for='3'>Première ligne de données</label>");
								if(!isset($dataImport["first_data_line"]))
									$dataImport["first_data_line"]="2";
								print('<input class=\'col-md-3\' id="3" type ="number" name="first_data_line" style="background: #dceeff" value='.$dataImport["first_data_line"].'>');
								print("</div>");
								print("</div>");
								print("</div>");

								print("<div class='row' title='Dans quelle colonne du fichier Excel source se trouvent les données correspondantes'><h4>Positionnement des données</h4></div>");

								print("<div class='row'>");
								print("<div class='col-md-4' style='background: #ececec'>");
								print("<label class='col-md-8' for='5' >Quantité unitaire</label>");
								if(!isset($dataImport["quantity_col"]))
									    $dataImport["quantity_col"]="NULL";
								print('<select class="col-md-4" id="5" name="quantity_col" title="Colonne Excel correspondant à la quantité unitaire">');
								columnOptionList("ZZ",true,$dataImport["quantity_col"]);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4'>");
								print("<label class='col-md-8' for='6'>Repère</label>");
								if(!isset($dataImport["rep_col"]))
									    $dataImport["rep_col"]="NULL";
								print('<select class="col-md-4" id="6" name="rep_col" title="Colonne Excel correspondant au repère">');
								columnOptionList("ZZ",true,$dataImport["rep_col"]);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4' style='background: #ececec'>");
								print("<label class='col-md-8' for='7'>Numéro de plan</label>");
								if(!isset($dataImport["planNum_col"]))
									    $dataImport["planNum_col"]="NULL";
								print('<select class="col-md-4" id="7" name="planNum_col" title="Colonne Excel correspondant au numéro de plan">');
								columnOptionList("ZZ",true,$dataImport["planNum_col"]);
								print("</select>");
								print("</div>");
								print("</div>");

								print("<div class='row'>");
								print("<div class='col-md-4'>");
								print("<label class='col-md-8' for='8'>Indice</label>");
								if(!isset($dataImport["indice_col"]))
									    $dataImport["indice_col"]="NULL";
								print('<select class="col-md-4" id="8" name="indice_col" title="Colonne Excel correspondant à l\'indice de plan">');
								columnOptionList("ZZ",true,$dataImport["indice_col"]);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4' style='background: #ececec'>");
								print("<label class='col-md-8' for='9'>Désignation</label>");
								if(!isset($dataImport["designation_col"]))
									    $dataImport["designation_col"]="NULL";
								print('<select class="col-md-4" id="9" name="designation_col" title="Colonne Excel correspondant à la désignation">');
								columnOptionList("ZZ",true,$dataImport["designation_col"]);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4'>");
								print("<label class='col-md-8' for='12'>Type principal</label>");
								if(!isset($dataImport["type_col"]))
									    $dataImport["type_col"]="NULL";
								print('<select class="col-md-4" id="12"  name="type_col" title="Colonne Excel correspondant au type principal">');
								columnOptionList("ZZ",true,$dataImport["type_col"]);
								print("</select>");
								print("</div>");

								print("</div>");

								print("<div class='row'>");

								print("<div class='col-md-4' style='background: #ececec'>");
								print("<label class='col-md-8' for='13'>Sous-type (fab)</label>");
								if(!isset($dataImport["fab_col"]))
									    $dataImport["fab_col"]="NULL";
								print('<select class="col-md-4" id="13"  name="fab_col" title="Colonne Excel correspondant au type secondaire">');
								columnOptionList("ZZ",true,$dataImport["fab_col"]);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4'>");
								print("<label class='col-md-8' for='14'>Traitements de surface</label>");
								if(!isset($dataImport["trSur_col"]))
									$dataImport["trSur_col"]="NULL";
								print('<select class="col-md-4" id="14"  name="trSur_col" title="Colonne Excel correspondant au(x) traitement(s) de surface">');
								columnOptionList("ZZ",true,$dataImport["trSur_col"]);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4' style='background: #ececec'>");
								print("<label class='col-md-8' for='16'>Observations</label>");
								if(!isset($dataImport["ob_col"]))
									$dataImport["ob_col"]="NULL";
								print('<select class="col-md-4" id="16" name="ob_col" title="Colonne Excel correspondant aux observations">');
								columnOptionList("ZZ",true,$dataImport["ob_col"]);
								print("</select>");
								print("</div>");

								print("</div>");

								print("<div class='row'>");

								print("<div class='col-md-4' id='hierarchies' ");
								if($dataImport["import_mode"]==0)
								    print("style='display: none'");
								print(">");
								print("<label class='col-md-8' for='15'>Niveaux hiérarchiques</label>");
								if(!isset($dataImport["level_col"]))
									$dataImport["level_col"]="NULL";
								print('<select class="col-md-4" id="15"  name="level_col" title="Colonne Excel correspondant au niveaux de hiérarchie">');
								columnOptionList("ZZ",false,$dataImport["level_col"]);
								print("</select>");
								print("</div>");
								print("</div>");

								print("<div class='row'>");

								print("<div class='col-md-4' style='background: #ececec; '>");
								print("<label class='col-md-8' for='11'>Fabricants</label>");
								if(!isset($dataImport["fabricants_col"]))
									$dataImport["fabricants_col"]="NULL";
								print('<select class="col-md-4" style="height: 16rem" id="11"  name="fabricants_col[]" multiple title="Colonne Excel correspondant à/aux nom du/des fabricant(s)">');
								columnOptionList("ZZ",true,$dataImport["fabricants_col"],true);
								print("</select>");
								print("</div>");

								print("<div class='col-md-4'>");
								print("<label class='col-md-8' for='10'>Références</label>");
								if(!isset($dataImport["references_col"]))
									$dataImport["references_col"]="NULL";
								print('<select class="col-md-4" style="height: 16rem" id="10" name="references_col[]" multiple title="Colonne Excel correspondant à/aux référence(s) fabriquant(s)">');
								columnOptionList("ZZ",true,$dataImport["references_col"],true);
								print("</select>");
								print("</div>");

								print("</div>");
								print("</div>");

								/*End form*/
								print("<div class='form-group col-md-4 col-md-offset-2'>
                                    <button class=\"form-group col-md-12 au-btn au-btn-icon au-btn--green au-btn--small\"  type=\"submit\" name=\"import-data\" value='" . $_POST["edit-import"] . "'>
                                        <i class=\"zmdi \"></i>Envoyer</button></div>
                                <div class='form-group col-md-4'>
                                    <button class=\"form-group col-md-12 au-btn au-btn-icon au-btn--blue au-btn--small\"  type=\"submit\" name=\"import-data\" value='Cancel'>
                                        <i class=\"zmdi \"></i>Annuler</button></div>
                            </form>
                        </div>
                    </div>");
							}
							?>

                <div class="login-panel panel default-panel">
                    <div class="panel-heading">
                        <!-- CHART-->
                        <h3 class="panel-title">Imports</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row topnav">
                            <div class="search-container" style="margin: 15px">
                                <i class="fa fa-search col-md-3"> </i>
                                <input class="col-md-9 col-md-offset-1" type="text" id="myInput" onkeyup="searchIn()" placeholder="Rechercher..." title="Taper pour rechercher">
                            </div>
                        </div>

                        <div class="tableFixHead">
                            <table>
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nom d'import</th>
                                    <th>Hiérarchisation</th>
                                    <th>Couleur</th>
                                    <th>Edit</th>
                                    <th><i class='fa fa-trash fa-2x'></i></th>
                                </tr>
                                </thead>
                                <tbody id="tableContent">
                                <?php
                                $res=$db->select(array("*"), array("import_configuration"), array(),0,"id");
                                foreach ($res as $row) {
                                    print("<tr>");
                                    printTd($row["id"]);
                                    printTd($row["nom"]);
                                    $hierarchisation="Fichiers multiples, un par ensemble";
                                    if($row["import_mode"]==1)
                                        $hierarchisation="Fichier unique, description hiérarchisée";
                                    printTd("Mode ".$row["import_mode"]." : ".$hierarchisation);
                                    print("<td><div class=\"color-box\" style=\"background-color: ".$row["color"].";\"></div></td>");
                                    print("<td><form method='post' action='configurateur.php'>
                                                    <button type='submit' class='form-group au-btn--small' name='edit-import' value='".$row["id"]."'>
                                                        <div id='LoadIcon' class=\"fas fa-cog\" onmouseover='spin()' onmouseleave='unspin()'></div>
                                                    </button></form></td>");
                                    print("<td><form method=\"post\" action=\"configurateur.php\">
                                            <button title='Supprimer la configuration' class=\"form-group au-btn au-btn--red au-btn--small\"  type=\"submit\" name='delete'   value=\"".$row["id"]."\"
                                onclick=\"return confirm('Vous êtes sur le point de supprimer la configuration suivante : ".str_replace("'","\\'",$row["nom"])."');\">
                                                <i class=\"fa fa-remove\"></i>
                                            </button>
                                            </form></td></tr>");
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                        <form method="post" action="configurateur.php">
                            <button style="margin: auto" class="col-md-12 login-panel panel default-panel" type="submit" name="edit-import" value="new">
                                <div class="panel-body" style="text-align: center">
                                    <i class="fas fa-plus-circle"></i>
                                    Nouvel import
                                </div>
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include ("../controller/scripts.html");?>
<script>
    function levelShowHide() {
        if (document.getElementById(1).checked){
            document.getElementById("hierarchies").style.display = "initial";
        }//display
        else{
            document.getElementById("hierarchies").style.display = "none";
        }//hide
    }
</script>
</body>
</html>
<!-- end document-->