<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include("../controller/head.html") ?>
    <?php
        if($_SESSION["authorizations"]->getLevel()==2)
            echo "<title>Gestion des comptes</title>";
        else
            header("my-account.php") ;
    ?>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <div class="page-content--bgf7 ">
        <!-- STATISTIC-->
        <section class="statistic statistic2 spacer">
            <div class="container">
                <div class="col-md-4">
                    <a class="col-md-12" href="accounts-manager.php">
                        <div class="statistic__item statistic__item--red">
                            <h2 class="number" >Gestion de comptes</h2>
                            <span class="desc">Modifier les comptes existants</span>
                            <div class="icon">
                                <i class="zmdi zmdi-accounts">
                                </i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="col-md-12" href="new-account.php">
                        <div class="statistic__item statistic__item--orange">
                            <h2 class="number" >Création de comptes</h2>
                            <span class="desc">Créer des comptes visiteurs, employés ou administrateur</span>
                            <div class="icon">
                                <i class="zmdi zmdi-account-add"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="col-md-12" href="configurateur.php">
                        <div class="statistic__item statistic__item--blue">
                            <h2 class="number" >Gestion d'imports</h2>
                            <span class="desc">Créer ou modifier des méthodes d'imports</span>
                            <div class="icon">
                                <i class="zmdi zmdi-code"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </section>
    </div>
</div>
<?php include("../controller/scripts.html") ?>
</body>
</html>
<!-- end document-->