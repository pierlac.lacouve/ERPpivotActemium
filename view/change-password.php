<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?php include("../controller/head.html") ?>
    <title>Changer mot de passe</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <!-- Formulaire de creation d'affaire-->
    <div class="container spacer2">
        <div class="row spacer2 "></div>
        <div class="col-md-6 col-md-offset-3">
            <!-- CHART-->
            <div class="login-panel panel default-panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Changer votre mot de passe :</h3>
                </div>
                <div class="panel-body">
                    <?php
                        if (isset($_SESSION['erreur1']))
                        {
                            print ("<div style=\"color: red\">");
                            echo $_SESSION['erreur1'];
                            unset($_SESSION['erreur1']);
                            print("</div>");
                        }
                        elseif($_SESSION["first_connection"]==1){
                            print ("<div style=\"color: red\">PREMIERE CONNECTION : Veuillez changer votre mot de passe</div>");
                        }
                    ?>
                    <form action="../controller/modify-password.php" method="post">
                        <div class="form-group">
                            <label>Ancien mot de passe *</label>
                            <input class="au-input au-input--full" type="password" name="password0" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label>Nouveau mot de passe *</label>
                            <input class="au-input au-input--full" type="password" name="password1" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label>Confirmez le mot de passe *</label>
                            <input class="au-input au-input--full" type="password" name="password2" placeholder="" required>
                        </div>
                        <button class="form-group col-md-8 au-btn au-btn--block au-btn--green m-b-20" type="submit">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("../controller/scripts.html") ?>

</body>

</html>
<!-- end document-->
