<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";


if(isset($_POST['delete'])){
	$db->delete("users",array("login='".$_POST["delete"]."'"));
}
if(isset($_POST['edit-data']) && $_POST['edit-data']!='Cancel'){
	$db->update("users",array("authorizations"),array($_POST["role"]),array("login='".$_POST["edit-data"]."'"));
}

$ps = $db->select(array("login", "firstname", "lastname","fonction","authorizations"),array("users"),array(),0);

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
    <?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>Modifier des comptes</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php") ?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <!-- Formulaire de creation d'affaire-->
    <div class="container spacer2">
        <div class="row spacer2 ">
            <div class="col-md-10 col-lg-offset-1">
                <?php
                if(isset($_POST['reinitialize'])){
                    $res = $db->select(array("login", "firstname", "lastname","fonction","authorizations"),array("users"),array("login='".$_POST['reinitialize']."'"));
                    $newPass=randomPassword();
                    $db->update("users",array("password","first_connection"),array(password_hash($newPass,PASSWORD_BCRYPT),"1"),array("login='".$_POST['reinitialize']."'"));
                    $message = "<p>Attention, vous avez réinitialisé le compte de ".$res["firstname"]." ".$res["lastname"].".</p><p>Voici le nouveau mot de passe :</p><textarea class='col-md-12' style='text-align: center; resize: none' readonly>".$newPass."</textarea>";
                    print "<div class=\"panel panel-default\" style=\"margin-top:5%\">
                    <div class=\"panel-heading\">
                        <h1 class=\"panel-title\">Modification</h1>
                    </div>
                    <div class=\"panel-body\">
                    <h3 style='color: red'>".$message."</h3> 
                    </div>
                    </div>";
                }

								if(isset($_POST['change-level'])){
								    $dataUser=$db->select(array("*"),array("users"),array("login='".$_POST['change-level']."'"));
								    print('
                                    <div class="panel panel-default" style="margin-top:5%">
                                        <div class="panel-heading">
                                            <h1 class="panel-title">Changer les droits de '.$dataUser["firstname"].' '.$dataUser["lastname"].'</h1>
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" action="accounts-manager.php">
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-3">
                                                <label class="col-md-6" for="0">Niveau d\'autorisation</label>
                                                <select class="col-md-6" name="role" id="0">');
								    print('
                                                    <option value="2"');
								    if($dataUser["authorizations"]==2) {print(' selected');}
                                                                print('>Administrateur</option>
                                                    <option value="1"');
								    if($dataUser["authorizations"]==1) {print(' selected');}
                                                                print('>Utilisateur</option>
                                                    <option value="0"');
								    if($dataUser["authorizations"]==0) {print(' selected');}
                                                                print('>Visiteur</option>
                                                   ');
                                  print('
</select>
</div>                                            
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-4 col-md-offset-2">
                                                    <button class="form-group col-md-12 au-btn au-btn-icon au-btn--green au-btn--small"  type="submit" name="edit-data" value=' . $_POST["change-level"] . '>
                                                        <i class="zmdi "></i>Envoyer</button></div>
                                                <div class=\'form-group col-md-4\'>
                                                    <button class="form-group col-md-12 au-btn au-btn-icon au-btn--blue au-btn--small"  type="submit" name="edit-data" value=\'Cancel\'>
                                                        <i class="zmdi "></i>Annuler</button></div></div>
                                            </form>
                                        </div>
                                    </div>
                                  ');
								}

                ?>
                <div class="panel panel-default" style="margin-top:5%">
                    <div class="panel-heading">
                        <h1 class="panel-title">Comptes</h1>
                    </div>
                    <div class="panel-body">

                        <div class="row topnav">
                            <div class="search-container" style="margin: 15px">
                                <i class="fa fa-search col-md-1"> </i>
                                <input class="col-md-9 col-md-offset-1" type="text" id="myInput" onkeyup="searchIn()" placeholder="Rechercher..." title="Taper pour rechercher">
                            </div>
                        </div>

                        <form method="post" action="accounts-manager.php">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Login</th>
                                    <th>Prénom</th>
                                    <th>Nom</th>
                                    <th>Fonction</th>
                                    <th>Type de compte</th>
                                    <th colspan="3"></th>
                                </tr>
                                </thead>
                                <tbody id="tableContent">
                                <?php
                                while ($data=$ps->fetch())
                                {
                                    print("<tr>");
                                    print("<td id='edit'>".$data['login']."</td>");
                                    print("<td id='edit'>".$data['firstname']."</td>");
                                    print("<td id='edit'>".$data['lastname']."</td>");
                                    print("<td>".$data['fonction']."</td>");
                                    switch ($data['authorizations'])
                                    {
                                        case '2':
                                            print("<td>Administrateur</td>");
                                            break;
                                        case '1':
                                            print("<td>Utilisateur</td>");
                                            break;
                                        case '0':
                                            print("<td>Visiteur</td>");
                                            break;
                                    }
                                    print ("<td><button title='Réitinialiser le mot de passe' class=\"form-group au-btn au-btn--blue au-btn--small\"  type=\"submit\" name='reinitialize'   value=\"".$data["login"]."\"
                                onclick=\"return confirm('Vous êtes sur le point de réinitialiser le mot de passe de ".$data['firstname']." ".$data['lastname'].", êtes vous sûr ?');\">
                                                <i class=\"fa fa-redo\"></i>
                                            </button></td>");
                                    print ("<td><button title='Editer les droits du compte' class=\"form-group au-btn au-btn--green au-btn--small\"  type=\"submit\" name='change-level'   value=\"".$data["login"]."\">
                                                <i class=\"fa fa-pencil-alt\"></i>
                                            </button></td>");
                                    print ("<td><button title='Supprimer le compte' class=\"form-group au-btn au-btn--red au-btn--small\"  type=\"submit\" name='delete'   value=\"".$data["login"]."\"
                                onclick=\"return confirm('Vous êtes sur le point de supprimer le compte \'".$data['firstname']." ".$data['lastname']."\', êtes vous sûr ?');\">
                                                <i class=\"fa fa-remove\"></i>
                                            </button></td>");
                                    
                                    print("</tr>");
                                }
                                ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include ("../controller/scripts.html");?>
</body>
</html>
<!-- end document-->