<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
$conditions_text = array();
$conditions = array();
if(isset($_SESSION["filters"]) && empty($_POST)){
    $_POST=$_SESSION["filters"];
    unset($_SESSION["filters"]);
}
if((isset($_POST["filtre-send"]) && $_POST["filtre-send"]=="Send" && !isset($_POST["hierarchiser"]))) {

    $conditions[] = createPivotCondition("h.id_affaire", $_POST["Affaires"]);
    $conditions[] = createPivotCondition("h.id_parent", $_POST["Parents"]);
    $conditions[] = createPivotCondition("h.id_self", $_POST["Self"]);
    $conditions[] = createPivotCondition("f.id", $_POST["Fabricants"]);
    $conditions[] = createPivotCondition("h.statut", $_POST["Statuts"]);
    $conditions[] = createPivotCondition("e1.type", $_POST["Types"]);
    $conditions[] = createPivotCondition("e1.fabrication", $_POST["TypeFab"]);
    $conditions[] = createPivotCondition("e1.traitement_surface", $_POST["TrSur"]);
    $result = $db->export_filtered($conditions);
}
else{

    if(isset($_POST["Affaires"]) && ($_POST["Affaires"][0]!="")){$conditions_text[] = "Affaire-". implode(";",$_POST["Affaires"]);}
    if(isset($_POST["Parents"]) && ($_POST["Parents"][0]!="")){$conditions_text[] = "Parent-". implode(";",$_POST["Parents"]);}
    if(isset($_POST["Self"]) && ($_POST["Self"][0]!="")){$conditions_text[] = "Pièce-". implode(";",$_POST["Self"]);}
    if(isset($_POST["Fabricants"]) && ($_POST["Fabricants"][0]!="")){$conditions_text[] = "Fabricant-". implode(";",$_POST["Fabricants"]);}
    if(isset($_POST["Statuts"]) && ($_POST["Statuts"][0]!="")){$conditions_text[] = "Statut-". implode(";",$_POST["Statuts"]);}
    if(isset($_POST["Types"]) && ($_POST["Types"][0]!="")){$conditions_text[] = "Type-". implode(";",$_POST["Types"]);}
    if(isset($_POST["TypeFab"]) && ($_POST["TypeFab"][0]!="")){$conditions_text[] = "Fabrication-". implode(";",$_POST["TypeFab"]);}
    if(isset($_POST["TrSur"]) && ($_POST["TrSur"][0]!="")){$conditions_text[] = "Traitement-surface-". implode(";",$_POST["TrSur"]);}
    if(!isset($_POST["hierarchiser"])){
        unset($_POST);
        $affaires_user=$db->select(array("a.id"),array("affaires a"),array("a.login_chef_projet='".$_SESSION["login"]."'"),0);
        $_POST["Affaires"]=array();
        while($affaire_user_it=$affaires_user->fetch()){
            $_POST["Affaires"][]=$affaire_user_it["id"];
        }
    }
    $result=$db->export_pivot(array(createPivotCondition("h.id_affaire", $_POST["Affaires"])));
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<!--HEAD IMPORT-->
	<?php include("../controller/head.html");?>
    <script src='../js/jquery-3.0.0.js' type='text/javascript'></script>
    <script src='../js/updateScript.js' type='text/javascript'></script>
	<!-- Title Page-->
	<title>Pivot</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->
<script>
    function display_hide_filters() {
        var filters = document.getElementsByClassName("hideable_filter");
        var i;
        for (i = 0; i < filters.length; i++) {
            if (document.getElementById("8").checked) {
                filters[i].style.display  = "none";
            } else {
                filters[i].style.display  = "inline";
            }
        }
    }
</script>
<style>
    .hideable_filter{
    <?php if(isset($_POST["hierarchiser"])) print("display: none");else print("display: inline") ?>
    }

    .collapsible {
        cursor: pointer;
        border: black;
        outline: none;
    }

    .content {
        display: none;
        overflow: hidden;
        border: black 1px;
    }
</style>

<div class="page-wrapper">
	<!-- PAGE CONTENT-->
	<div class="container spacer2">
		<div class="row spacer2 ">
			<div class="col-lg-12 ">
				<!-- Advanced Tables -->
                <div class="login-panel panel default-panel">
					<div class="panel-heading">
						<h3 class="panel-title">Pivot</h3>
					</div>
                    <div class="panel-body">
                        <div class="row topnav">
                            <div class="col-md-4 search-container">
                                <i class="fa fa-search col-md-1"> </i>
                                <input class="col-md-9 col-md-offset-1" type="text" id="myInput" onkeyup="searchIn()" placeholder="Rechercher..." title="Taper pour rechercher">
                            </div>
                            <div class="col-md-7 col-md-offset-1">
                                <div class="panel-heading collapsible"><h4>Filtres</h4></div>
                                <div class="content">
                                    <div class="login-panel panel default-panel">
                                        <div class="panel-body">

                                            <form method="post" action="../view/pivot.php" enctype="multipart/form-data">
                                                <div class="form-row">
                                                    <div class="col-md-6">
                                                        <label for="0">Affaire(s)</label>
                                                        <select name="Affaires[]" class="form-control form-control-lg" id="0" multiple="multiple">
                                                            <?php
                                                            $dataTemp=$db->getTable("affaires a join hierarchies h on a.id=h.id_affaire","a.id","a.designation");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                print("<option value='".$row["ID"]."'");
                                                                if(isset($_POST["Affaires"]) && in_array($row["ID"],$_POST["Affaires"]))
                                                                    print("selected");
                                                                print(">".$row["ID"]." - ".$row["NAME"]."</option>");
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                        <hr>
                                                        <label for="8">Hiérarchiser</label>
                                                        <input class="checkbox col-md-1" id="8" type="checkbox" name="hierarchiser" <?php if(isset($_POST["hierarchiser"])) print("checked"); ?> onclick=display_hide_filters()>

                                                    </div>
                                                    <div class="hideable_filter col-md-6">
                                                        <label for="1">Parent(s)</label>
                                                        <select name="Parents[]" class="form-control form-control-lg" id="1" multiple="multiple" style="height: 15rem">
                                                            <?php
                                                            $dataTemp=$db->getTable("elements","id","CONCAT(IFNULL(`numero_plan`,\"[non renseigné]\"),'-',IFNULL(`indice`,\"[non renseigné]\"),' - ',IFNULL(`designation`,\"[non renseigné]\"))","id IN (SELECT DISTINCT id_parent FROM hierarchies)");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                print("<option value='".$row["ID"]."'");
                                                                if(isset($_POST["Parents"]) && in_array($row["ID"],$_POST["Parents"]))
                                                                    print("selected");
                                                                print(">".$row["NAME"]."</option>");
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="hideable_filter form-row">
                                                    <div class="col-md-6">
                                                        <label for="2">Numéro de plan</label>
                                                        <select name="Self[]" class="form-control form-control-lg" id="1" multiple="multiple" style="height: 15rem">
                                                            <?php
                                                            $dataTemp=$db->getTable("elements","id","CONCAT(IFNULL(`numero_plan`,\"[non renseigné]\"),'-',IFNULL(`indice`,\"[non renseigné]\"),' - ',IFNULL(`designation`,\"[non renseigné]\"))","numero_plan IS NOT NULL");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                print("<option value='".$row["ID"]."'");
                                                                if(isset($_POST["Self"]) && in_array($row["ID"],$_POST["Self"]))
                                                                    print("selected");
                                                                print(">".$row["NAME"]."</option>");
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="3">Fabricant(s)</label>
                                                        <select name="Fabricants[]" class="form-control form-control-lg" id="3" multiple="multiple" style="height: 15rem">
                                                            <?php
                                                            $dataTemp=$db->getTable("Fabricants","id","nom","1 ORDER BY nom ASC");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                print("<option value='".$row["ID"]."'");
                                                                if(isset($_POST["Fabricants"]) && in_array($row["ID"],$_POST["Fabricants"]))
                                                                    print("selected");
                                                                print(">".$row["NAME"]."</option>");
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="hideable_filter form-row">
                                                    <div class="col-md-6">
                                                        <label for="4">Statut(s)</label>
                                                        <select name="Statuts[]" class="form-control form-control-lg" id="4" multiple="multiple">
                                                            <?php
                                                            $dataTemp=$db->getTable("hierarchies","statut");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                if($row["ID"]!="")
                                                                {
                                                                    print("<option value='".$row["ID"]."'");
                                                                    if(isset($_POST["Statuts"]) && in_array($row["ID"],$_POST["Statuts"]))
                                                                        print("selected");
                                                                    print(">".$row["ID"]."</option>");
                                                                }
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="5">Traitement(s) de surface</label>
                                                        <select name="TrSur[]" class="form-control form-control-lg" id="5" multiple="multiple">
                                                            <?php
                                                            $dataTemp=$db->getTable("elements","traitement_surface");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                if($row["ID"]!="")
                                                                {
                                                                    print("<option value='".$row["ID"]."'");
                                                                    if(isset($_POST["TrSur"]) && in_array($row["ID"],$_POST["TrSur"]))
                                                                        print("selected");
                                                                    print(">".$row["ID"]."</option>");
                                                                }
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="hideable_filter form-row">
                                                    <div class="col-md-6">
                                                        <label for="6">Type(s)</label>
                                                        <select name="Types[]" class="form-control form-control-lg" id="6" multiple="multiple">
                                                            <?php
                                                            $dataTemp=$db->getTable("elements","type");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                if($row["ID"]!=""){
                                                                    print("<option value='".$row["ID"]."'");
                                                                    if(isset($_POST["Types"]) && in_array($row["ID"],$_POST["Types"]))
                                                                        print("selected");
                                                                    print(">".$row["ID"]."</option>");
                                                                }
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="7">Type(s) Fab</label>
                                                        <select name="TypeFab[]" class="form-control form-control-lg" id="7" multiple="multiple">
                                                            <?php
                                                            $dataTemp=$db->getTable("elements","fabrication");
                                                            print("<option selected value=\"\"></option>");

                                                            while ($row=$dataTemp->fetch()){
                                                                if($row["ID"]!=""){
                                                                    print("<option value='".$row["ID"]."'");
                                                                    if(isset($_POST["TypeFab"]) && in_array($row["ID"],$_POST["TypeFab"]))
                                                                        print("selected");
                                                                    print(">".$row["ID"]."</option>");
                                                                }
                                                            }
                                                            unset($dataTemp);
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row spacer">
                                                    <button class="form-group col-md-4 col-md-offset-2 au-btn au-btn-icon au-btn--green au-btn--small"  type="submit" name="filtre-send" value="Send" onclick="
                                                    if(document.getElementById('8').checked){
                                                        return confirm('Attention la hiérarchisation des données est une opération lourde, si la quantité de donnée à renvoyer est trop grande, il est possible que l\'application ne réponde pas.\n\nVoulez vous continuer ?')
                                                    }
                                                    "><i class="zmdi"></i>Envoyer</button>
                                                    <button class="form-group col-md-4 col-md-offset-1 au-btn au-btn-icon au-btn--red au-btn--small"  type="submit" name="filtre-send" value="Cancel"><i class="zmdi"></i>Reset</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="tableFixHead" style="height: 450px">
                                <table>
                                    <thead style="background-color:#0d70b7">
                                        <tr>
                                            <th style="width: 10%">Affaire</th>
                                            <th style="width: 10%">S/Ens ou poste</th>
                                            <th>REP</th>
                                            <th style="width: 10%">Numéro Plan</th>
                                            <th><i class="fa fa-pencil-alt"></i>Ind Plan</th>
                                            <th style="width: 10%"><i class="fa fa-pencil-alt"></i>Designation</th>
                                            <th><i class="fa fa-pencil-alt"></i>Qté unit</th>
                                            <th>Qté totale</th>
                                            <th>Type</th>
                                            <th style="width: 10%"><i class="fa fa-pencil-alt"></i>Fab</th>
                                            <th style="width: 10%">Traitements de surface</th>
                                            <th style="width: 10%">Fabricant - Référence</th>
                                            <th><i class="fa fa-pencil-alt"></i>Statut</th>
                                            <th style="width: 10%"><i class="fa fa-pencil-alt"></i>Observation</th>
                                            <?php
                                            if($_SESSION["authorizations"]->getLevel()>0)
                                                print("<th><i class='fa fa-trash fa-2x'></i></th>");
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody id="tableContent">
                                    <?php
                                    $conditions_statut=array();
                                    foreach ($result as $row) {
                                        $conditions_statut[]=implode("#",array($row["Affaire"],$row["IdParent"],$row["Id"]));
                                        print "<tr>";
                                        printTd($row["Affaire"]); // Affaire
                                        printTd($row["Parent"]); // Parent
                                        printTd($row["Repere"]); // Repère
                                        printTd($row["NumPlan"]); // Numéro de Plan
                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["Indice"],"elements#indice#id='" . $row["Id"]); // Indice
                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["Designation"],"elements#designation#id=" . $row["Id"]); // Désignation
                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["QteU"],"hierarchies#quantity#id_parent=".$row["IdParent"].";id_self=".$row["Id"].";id_affaire=" . $row["Affaire"]); // Quantité unitaire
                                        printTd($row["QteT"]); // Type d'élément
                                        printTd($row["EltType"]); // Type d'élément
                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["Fab"],"elements#fabrication#id=" . $row["Id"]); // Fabrication
                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["Traitements_Surface"],"elements#traitement_surface#id=" . $row["Id"]); // Traitement de surface

                                        $resultNum = intval($db->select(array("count(*)"), array("Fabricants_elements fe"), array("fe.id_element=" . $row["Id"]))[0]);
                                        $req2 = $db->select(array("fe.reference", "f.nom","f.id"), array("Fabricants_elements fe", "Fabricants f"),
                                                        array("f.id=fe.id_Fabricant", "fe.id_element=" . $row["Id"]), 0);
                                        print("<td><div>");
                                        for ($i=0;$i<$resultNum;++$i){
                                            $data= $req2->fetch();
                                            print("<p>".$data[1] . " - </p><p style='font-style: oblique '> " . $data[0]."</p>");
                                        }
                                        print("</div></td>");

                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["Statut"],"hierarchies#statut#id_parent=".$row["IdParent"].";id_self=".$row["Id"].";id_affaire=" . $row["Affaire"]); // Traitement de surface
                                        printTdEditable($_SESSION["authorizations"]->getLevel(),$row["Obs"],"hierarchies#observation#id_parent=".$row["IdParent"].";id_self=".$row["Id"].";id_affaire=" . $row["Affaire"]); // Observation

                                        if($_SESSION["authorizations"]->getLevel()>0){
                                            print("<td>");

                                            $ensWarning="";
                                            if($row["EltType"]=="ens"){
                                                $ensWarning=" !!! ATTENTION !!! L\'ELEMENT SELECTIONNE EST UN ENSEMBLE !!! TOUS LES DESCENDANTS DE CET ENSEMBLE SERONT SUPPRIMES EGALEMENT !!!";
                                            }
                                            print("<form method=\"post\" action=\"../controller/delete-single-row.php\">
                                            <button title='Supprimer la ligne' class=\"form-group au-btn au-btn--red au-btn--small\"  type=\"submit\" name='delete-row'   value=\"".$row["Affaire"]."#".$row["Id"]."#".$row["IdParent"]."\"
                                onclick=\"return confirm('Vous êtes sur le point de supprimer l\'élément suivant : ".str_replace("'","\\'",$row["NumPlan"])." - ".str_replace("'","\\'",$row["Designation"]).$ensWarning."');\">
                                                <i class=\"fa fa-remove\"></i>
                                            </button>
                                            </form>");
                                            print("</td>");
                                        }

                                        print "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                          <?php
                            if(isset($_POST["filtre-send"]) && $_POST["filtre-send"]=="Send"){
                                print("<div class=\"col-md-2 col-md-offset-2\">");
                                if(!isset($_POST["hierarchiser"])) {
                                    print("<form method=\"post\" action=\"../controller/export-pivot.php\">");

                                    print("<input type='text' style='display: none' name='conditions' value=\"" . implode(";", $conditions) . "\"/>");
                                    print("<input type='text' style='display: none' name='condition_text' value=\"" . implode("_", $conditions_text) . "\"/>");
                                    print("<button style=\"margin: auto\" class=\"col-md-12\" type=\"submit\" name=\"Import-pivot\">
                                            <div class=\"panel-body form-group au-btn au-btn--green au-btn--small\" style=\"text-align: center; margin-top: 1.5rem\">
                                                Télécharger
                                            </div>
                                        </button>
                                        </form>");
                                }
                                else{
                                    print("<p>Extraction de pivot (hierarchisé) sur la page d'<a href='accueil.php'>accueil</a></p>");
                                }
                                print("</div>");
                            }
                          ?>
                            <div class="col-md-2 col-md-offset-<?php if(isset($_POST["filtre-send"]) && $_POST["filtre-send"]=="Send"){print("1");}else{print("5");}?>">
                                <form method="post" action="./pivot.php">
                                    <button style="margin: auto" class="col-md-12 login-panel panel default-panel" onmouseover="spin()" onmouseleave="unspin()" type="submit" name="loadAff">
                                        <div class="panel-body" style="text-align: center">
                                            <i class="fas fa-sync" id="LoadIcon"></i>
                                            Actualiser
                                        </div>
                                    </button>
                                </form>
                            </div>
                          <?php
                          if(isset($_POST["filtre-send"]) && $_POST["filtre-send"]=="Send"){
                                print("<div class=\"col-md-4\">
                                    <form method=\"post\" action=\"../controller/update.php\">");
                                    print("<input type='text' style='display: none' name='conditions' value=\"" . implode(";", $conditions_statut) . "\"/>");
                                    $hier=0;
                                    if(isset($_POST["hierarchiser"]))
                                        $hier=1;
                                    print("<input type='text' style='display: none' name='hierarchiser' value=\"" . $hier . "\"/>");
                                    print("<div class=\"col-md-8\" style='margin-top: 2.5rem;'>");
                                        print("<label class='col-md-4' for='statut'>Statut :</label>");
                                        print("<select class='col-md-8' id='statut' name='statut'>");
                                        print("<option value='NULL'>-</option>");
                                        print("<option value='EN ETUDE'>EN ETUDE</option>");
                                        print("<option value='BON POUR CONSULT'>BON POUR CONSULT</option>");
                                        print("<option value='BON POUR APPRO/FAB'>BON POUR APPRO/FAB</option>");
                                        print("</select>");
                                   print("</div>");
                                    print("<div class=\"col-md-4\">");
                                    print("<button title='Envoyer un statut à toutes les pièces filtrées' class=\"col-md-8 panel-body form-group au-btn au-btn--blue au-btn--small\" style=\"text-align: center; margin-top: 1.5rem\" type=\"submit\" name=\"Status-Update\">
                                            <div >
                                                <i class=\"fa fa-send\"></i>
                                            </div>
                                        </button>
                                        </div>
                                </form>
                            </div>");

                                print("<div class=\"col-md-1\">
                                <form method=\"post\" action=\"../controller/delete-multiple-rows.php\">");
                                print("<input type='text' style='display: none' name='conditions' value=\"" . implode(";", $conditions_text) . "\"/>");
                                print("<button title='Supprimer toutes les lignes sélectionnées' class=\" col-md-8 panel-body form-group au-btn au-btn--red au-btn--small\" style=\"text-align: center; margin-top: 1.5rem\" onclick='return confirm(\"!!! VOUS ÊTES SUR LE POINT DE SUPPRIMER TOUTES DES DONNEES EN MASSE, VOULEZ VOUS CONTINUER ? !!!\")' type=\"submit\" name=\"Delete-conditions\">
                                        <div >
                                            <i class=\"fa fa-remove\"></i>
                                        </div>
                                    </button>
                                </form>
                            </div>");
													}
                          ?>

                            <?php
                            if (isset($_SESSION['erreur']) || isset($_SESSION['OK']) || isset($_SESSION["database_log"])) {
                                print("<div style=\"color: ");
                                if (isset($_SESSION['erreur'])) {
                                    print("red;height:100px;overflow:auto;\">");
                                    print_arr($_SESSION['erreur']);
                                    unset($_SESSION['erreur']);
                                } elseif (isset($_SESSION['OK'])) {
                                    print("green;text-align: center\">");
                                    echo $_SESSION['OK'];
                                    unset($_SESSION['OK']);
                                }
                                print("</div>");
                            }
                            ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<?php include ("../controller/scripts.html");?>
<script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }
</script>
</body>
</html>
<!-- end document-->