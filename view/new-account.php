<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
    <title>Gestion des comptes</title>
    <?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>Nouveau compte</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php");?>
<!-- END HEADER DESKTOP -->

<div class="page-wrapper">
    <!-- Formulaire de creation d'affaire-->
    <div class="container spacer2">
        <div class="row spacer2 "></div>
            <div class="col-md-6 col-md-offset-3">
                <!-- CHART-->
                <div class="login-panel panel default-panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Saisir les informations</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        if (isset($_SESSION['erreur']))
                        {
                            print ("<div style=\"color: red\">");
                            print_arr($_SESSION['erreur']);
                            unset($_SESSION['erreur']);
                            print("</div>");
                        }
                        if (isset($_SESSION['success1']))
                        {
                            print ("<div style=\"color: green\">");
                            echo $_SESSION['success1'];
                            unset($_SESSION['success1']);
                            print("</div>");

                            print ("<div style=\"color: green;font-family: 'Courier New'\">");
                            echo $_SESSION['success2'];
                            unset($_SESSION['success2']);
                            print("</div>");
                            print("<div>ATTENTION LE MOT DE PASSE NE SERA PLUS ACCESSIBLE UNE FOIS CETTE PAGE QUITTEE</div>");
                        }
                        ?>
                        <form method="post" action="../controller/insert-account.php" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="0">Nom *</label>
                                    <input type="text" name="lastname" class="form-control" id="0" placeholder="Nom" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="1">Prénom *</label>
                                    <input type="text" name="firstname" class="form-control" id="1" placeholder="Prénom" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="2">Adresse e-mail *</label>
                                    <input type="text" name="email" class="form-control" id="2" placeholder="prenom.nom@exemple.eg" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="4">Droits *</label>
                                    <select name="credentials" class="form-control form-control-lg" id="4" required>
                                        <option value="2">Administrateur</option>
                                        <option value="1">Utilisateur</option>
                                        <option value="0" selected="selected">Visiteur</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="3">Fonction</label>
                                    <select name="job" class="form-control form-control-lg" id="3" required>
                                        <?php
                                        $dataTemp=$db->getTable("fonctions","intitule");
                                        print("<option selected value=\"NULL\">-</option>");
                                        while ($row=$dataTemp->fetch()){
                                            if($row["ID"]!=""){
                                                print("<option value='".$row["ID"]."'");
                                                if(isset($_POST["TypeFab"]) && in_array($row["ID"],$_POST["TypeFab"]))
                                                    print("selected");
                                                print(">".$row["ID"]."</option>");
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                            <button class="form-group col-md-8 col-md-offset-2 au-btn au-btn-icon au-btn--green au-btn--small" type="submit" name="enregistrer">
                                <i class="zmdi "></i>Envoyer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END CHART-->
            </div>
        </div>
    </div>

</div>

<?php include ("../controller/scripts.html");?>
</body>
</html>
<!-- end document-->