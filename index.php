<?php include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title Page-->
    <title>Intégration et solutions industrielles</title>
    <!-- Core CSS - Include with every page -->
    <link rel="shortcut icon" href="assets/images/favicon.ico"/>
    <link rel="shortcut icon" href="assets/images/favicon.png"/>
    <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/main-style.css" rel="stylesheet" />
    <link href="css/fondEcran1.css" rel="stylesheet" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
</head>

<body style="background-image:url(assets/images/g.jpg); background-size: 100% ">
<div class="container spacer2">
    <div class="row spacer2 ">
        <div class="col-md-6 col-md-offset-3">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Bienvenue</h1>
                </div>
                <div class="panel-body ">
                    <form method="post" action="controller/authentication.php" enctype="multipart/form-data">
                        <fieldset>
                            <div>
                                    <!--b style="font-size:300%"> SIGN IN </b-->
                                <div>
                                    <img src="assets/images/facebook-cropped.jpg" class="img-center">
                                </div>

                                <div style="color: red">
                                    <?php
                                     if (isset($_SESSION['erreur']) || isset($_SESSION['OK'])) {
                                        print("<div style=\"color: ");
                                        if (isset($_SESSION['erreur'])) {
                                            print("red;height:100px;overflow:auto;\">");
                                            print_arr($_SESSION['erreur']);
                                            unset($_SESSION['erreur']);
                                        }
                                        print("</div>");
                                    }
                                    //print(date_format(($lastco),"d/m/Y H:MM:S"));
                                    if(isset($_SESSION["login"])) {
                                        $now = strtotime(date("Y-m-d H:i:s"));
                                        $lastco=strtotime($db->select(array("last_connection"), array("users"), array("login='" . $_SESSION["login"] . "'"))[0]);
                                        $diff = $now - $lastco;
                                        if (($_SESSION["rester_connecte"] && $diff > 24*3600) || (!$_SESSION["rester_connecte"] && $diff > 3600))
                                            print("Vous êtes resté inactif depuis le ".gmdate("d/m/Y",$lastco)." à ".gmdate("H:i",$lastco).", veuillez vous reconnecter.");
                                    }
                                    ?>
                                </div>

                            </div>
                            <div class="form-group ">
                                <label>Login</label>
                                <input class="form-control " placeholder="prénom.nom@actemium.com" name="username" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <label>Mot de passe</label>
                                <input class="form-control" placeholder="Mot de passe" name="password" type="password" value="">
                            </div>
                            <div class="form-group">
                                <label for="1">Rester connecté</label>
                                <input class="checkbox col-md-1" id="1" type="checkbox" name="rester_connecte">
                            </div>
                            <div>
                                <button class="btn btn-lg btn-success btn-block"  type="submit">Connexion</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Core Scripts - Include with every page -->
<?php require_once "controller/scripts.html"?>
</body>
</html>