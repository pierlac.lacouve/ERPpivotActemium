<?php
require_once("C:/wamp64/www/traitement-pivot/controller/database-connection.php");
require_once("C:/wamp64/www/traitement-pivot/Classes/AuthorizationLevel.php");

date_default_timezone_set('Europe/Paris');
mb_internal_encoding('UTF-8');
$login=$_POST['username'];
$PWD=$_POST['password'];

$ok=($userData=$db->select(array("*"),array("users"),array("login='".mb_strtolower($login)."'")));
if($ok && password_verify($PWD,$userData['password'])==1) {

	session_start();
	$_SESSION['first_connection'] = $userData['first_connection'];
	$_SESSION['fonction'] = $userData['fonction'];
	$_SESSION['authorizations'] = new AuthorizationLevel(intval($userData['authorizations']));
	$_SESSION['firstname'] = $userData['firstname'];
	$_SESSION['lastname'] = $userData['lastname'];
	$_SESSION['login'] = $userData['login'];
	$_SESSION['rester_connecte']=isset($_POST["rester_connecte"]);

	$db->update("users",array("last_connection"),array(date("Y-m-d H:i:s")),array("login='".mb_strtolower($login)."'"));
	header("location:../view/accueil.php");
}
else {
	session_start();
	$_SESSION['erreur'] = "Login ou mot de passe incorrect";
	header("location:../index.php");
}