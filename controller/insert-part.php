<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
$rep=$_POST['repere'];
$qte=$_POST['qte'];
$ind=$_POST['indice_plan'];
$des=$_POST['designation'];
$noP=$_POST['numPlan'];
$ref=$_POST['reference'];
$fab=$_POST['type_fabrication'];
$ens=$_POST['ensembles'];
$type=$_POST['id_domaine'];
$surface=$_POST['surface'];
$affaire=$_POST['affaire'];
$frn=$_POST['fabricants'];
if ($_SESSION['role']>0){
    header("location:../view/accueil.php");
}else {

	//GESTION DE L'INSERTION ET UPDATE DES PIECES (VIS, MECANIQUES, COMMERCE, MULTI-OPERATIONS)



	$db->insertPart("", $qte, $rep, $noP, $ind, $des, array($ref), array($frn), $fab, $type, "", $affaire);

	//PIECE :
	$pId = $db->getPartId($noP, $ref, $frn);
	//Metre à jour la piece si elle existe déjà, sinon, on j'ajoute.
	if ($pId != "") {
		$db->update("pieces", array("numero_plan", "designation", "type", "indice"), array($noP, $des, $type, $ind), array("id='" . $pId . "'"));
	} else {
		$pId = $db->insert("pieces", array("numero_plan", "designation", "type", "indice"), array($noP, $des, $type, $ind), 1);
	}


	//ARCHITECTURE DANS LES ENSEMBLES : Ajout de la pièce dans l'affaire si l'asociation n'existe pas déjà. OK
	//Ajout de la hierarchie entre pièce et ensemble
	if ($data1 = $db->select(array("id_piece_ensemble", "id_piece", "id_ensemble", "id_affaire"), array("pieces_ensembles"), array("id_piece='" . $pId . "'", "id_affaire='" . $affaire . "'", "id_ensemble='" . $parent . "'"))) {
		$db->update("pieces_ensembles", array("quantity", "id_plan", "indice", "repere"), array($qte, $noP, $ind, $rep), array("id_piece_ensemble='" . $data1["id_piece_ensemble"] . "'"));
	} else {
		$db->insert("pieces_ensembles", array("id_plan", "quantity", "indice", "repere", "id_piece", "id_ensemble", "id_affaire"), array($noP, $qte, $ind, $rep, $pId, $parent, $affaire));
		if ($frn != "NULL" && $ref != "NULL") {
			if (!($db->select(array("id", "nom"), array("fabricants"), array("nom='" . $frn . "'")))) {
				$db->insert("fabricants", array("nom"), array($frn));
			}
			$data3 = $db->select(array("id", "nom"), array("fabricants"), array("nom='" . $frn . "'"));

			$arrCond = array("id_piece='" . $pId . "'", "id_fabricant='" . $data3["id"] . "'");
			if (!($db->select(array("*"), array("fabricants"), $arrCond))) {
				$db->insert("pieces_fabricants", array("id_piece", "id_fabricant", "quantity", "reference"), array($pId, $data3["id"], $qte, $ref));
			} else {
				$db->update("pieces_fabricants", array("quantity", "reference"), array($qte, $ref), $arrCond);
			}
		}
	}
	header("location:../vue/achat.php?affaire=$affaire");
}