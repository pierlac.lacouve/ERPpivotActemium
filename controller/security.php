<?php
require_once("C:/wamp64/www/traitement-pivot/Classes/AuthorizationLevel.php");
require_once("C:/wamp64/www/traitement-pivot/controller/database-connection.php");
session_start();
date_default_timezone_set('Europe/Paris');
if(isset($_SESSION["login"])) {
	$now = strtotime(date("Y-m-d H:i:s"));
	$lastco = strtotime($db->select(array("last_connection"), array("users"), array("login='" . $_SESSION["login"] . "'"))[0]);
	$diff = $now - $lastco;
}
if(isset($_SESSION["authorizations"]) && ($diff<3600 || ($_SESSION["rester_connecte"] && $diff<24*3600))){
	$db->update("users",array("last_connection"),array(date("Y-m-d H:i:s")),array("login='".mb_strtolower($_SESSION["login"])."'"));
	if ($_SESSION["authorizations"]->isAuthorized() == false)
		header($_SESSION["authorizations"]->redirectTo());
}
elseif($_SERVER["PHP_SELF"]!="/traitement-pivot/index.php")
	header("Location:/traitement-pivot/index.php");
