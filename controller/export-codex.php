<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
/***** EDIT BELOW LINES *****/

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\StreamedResponse;

require_once '../PHPSpreadSheet/vendor/autoload.php';

if(isset($_POST["Import-codex"])) {
	ob_clean();

	$results = array_keys($_POST);

	$inputfilname = "C:/wamp64/www/traitement-pivot/assets/Base Codex.xlsx";

	$spreadsheet = new spreadsheet();

	try {
		ini_set('memory_limit', '16G');
		$inputfiletype = IOFactory::identify($inputfilname);
		$objReader = IOFactory::createReader($inputfiletype);
		$spreadsheet1 = $objReader->load($inputfilname);
	} catch (Exception $e) {
		die('Error loading file  "' . pathinfo($inputfilname, PATHINFO_BASENAME) . '": ' . $e->getMessage());
	}

	date_default_timezone_set('Europe/Paris');
	$filename = 'Export-YMM51-Date-' . date('d-m-Y-H-i-s') . '.xlsx'; // Define Excel (.xlsx) file name

	if ($results[0] != "Import-codex") {

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();
		$spreadsheet->addExternalSheet(clone $spreadsheet1->getSheetByName('Creation de DA'));

		// Set workbook properties
		$spreadsheet->getProperties()->setCreator('Actemium')
						->setLastModifiedBy('Actemium')
						->setTitle('YMM51');

		$sheetIndex = $spreadsheet->getIndex($spreadsheet->getSheetByName('Worksheet'));
		$spreadsheet->removeSheetByIndex($sheetIndex);
		//////////////////////////// FILE INSERTION HERE ////////////////////////////
		// Start while loop to get data
		$i = 20;
		foreach ($results as $row) {
			if (gettype($row) == "string" && $row != "Import-codex") {
				$data = explode("-", $row);
				$data[0] = str_replace("_", ".", $data[0]);
				if (in_array($data[2], $results)) {
					$piece = $db->select(array("e.designation", "fe.reference", "f.nom"),
									array("(elements e join fabricants_elements fe on e.id=fe.id_element) join fabricants f on f.id=fe.id_fabricant"),
									array("e.id='" . $data[2] . "'", "f.id='" . $_POST[$data[2]] . "'"));
					$texteDePoste = $piece[0] . " – Réf." . $piece[1] . " - " . $piece[2];
				} else {
					$piece = $db->select(array("e.designation", "e.numero_plan", "e.indice"), array("elements e"), array("e.id='" . $data[2] . "'"));
					$texteDePoste = $piece[0] . " suivant plan N°" . $piece[1] . "_Ind." . $piece[2];
				}

				$spreadsheet->getActiveSheet()->setCellValue("C" . $i, "U");
				$spreadsheet->getActiveSheet()->setCellValue("D" . $i, $piece[1]);
				$spreadsheet->getActiveSheet()->setCellValue("E" . $i, "EUR");
				$spreadsheet->getActiveSheet()->setCellValue("F" . $i, "0,01");
				$spreadsheet->getActiveSheet()->setCellValue("G" . $i, $_POST[$row]);
				$spreadsheet->getActiveSheet()->setCellValue("I" . $i, $texteDePoste);
				$spreadsheet->getActiveSheet()->setCellValue("M" . $i, "P");
				$spreadsheet->getActiveSheet()->setCellValue("N" . $i, $data[0]);
				++$i;
			}
		}

		$writer = IOFactory::createWriter($spreadsheet, "Xlsx");

		$response = new StreamedResponse(
						function () use ($writer) {
							$writer->save('php://output');
						}
		);
		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
		$response->headers->set('Content-Disposition', 'attachment;filename="' . $filename . '"');
		$response->headers->set('Cache-Control', 'max-age=0');
		$response->send();
	}
}