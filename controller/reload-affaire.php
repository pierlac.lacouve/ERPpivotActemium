<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
require_once '../PHPSpreadSheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
if(isset($_POST["loadAff"])) {
	$inputfilename = "C:\\wamp64\\www\\traitement-pivot\\assets\\Affaires\\Affaires.xlsx";

	try {
		$inputfiletype = IOFactory::identify($inputfilename);
		$objReader = IOFactory::createReader($inputfiletype);
		$spreadsheet = $objReader->load($inputfilename);
	} catch (Exception $e) {
		die('Error loading file  "' . pathinfo($inputfilename, PATHINFO_BASENAME) . '": ' . $e->getMessage());
	}

	$sheet = $spreadsheet->getActiveSheet();
	$highestRow = $sheet->getHighestRow();

	for ($line = 2; $line <= $highestRow; $line++) {
		unset($idClient,$des,$client);
		$idAff = getCellData("A" . $line);
		$des = getCellData("B" . $line);
		$client = getCellData("C" . $line);
		if ($idAff != "NULL") {
			if ($data=$db->select(array("*"), array("clients"), array("nom='" . $client."'"))) {
				$idClient=$data["id"];
			}
			elseif($client!="NULL"){
				$idClient=$db->insert("clients", array("nom"), array($client),1);
			}
			else{
				$idClient="NULL";
			}

			if (($data0=$db->select(array("*"), array("affaires"), array("id='" . $idAff."'")))) {
				$db->update("affaires",array("id_client","designation"),array($idClient,$des),array("id='".$data0["id"]."'"));
			}
			else{
				$db->insert("affaires",array("id","designation","id_client"),array($idAff,$des,$idClient));
				$inserted=true;
			}
		}
	}

	if(!isset($_SESSION["error"])){
		if(isset($inserted)){
			$_SESSION["OK"]="Actualisation réussie";
			unset($inserted);
		}
		else{
			$_SESSION["OK"]="Aucune nouvelle affaire";
		}
	}

	header("location: ../view/affaires.php");
}