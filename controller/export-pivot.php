<?php
include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
require_once '../PHPSpreadSheet/vendor/autoload.php';


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\StreamedResponse;

if(isset($_POST["Import-pivot"])) {
	ob_clean();

	$inputfilname = "C:/wamp64/www/traitement-pivot/assets/Base Pivot.xlsx";

	$spreadsheet = new spreadsheet();

	try {
		$inputfiletype = IOFactory::identify($inputfilname);
		$objReader = IOFactory::createReader($inputfiletype);
		$spreadsheet1 = $objReader->load($inputfilname);
	} catch (Exception $e) {
		die('Error loading file  "' . pathinfo($inputfilname, PATHINFO_BASENAME) . '": ' . $e->getMessage());
	}

	$dir = "../sheets/";

	date_default_timezone_set('Europe/Paris');
	$filename = 'Export-Pivot'; // Define Excel (.xlsx) file name
	if($_POST["Import-pivot"]=="Télécharger"){
		$conditions = array("h.id_affaire='". $_POST["affaire"]."'");
		$result=$db->export_pivot($conditions);
		$filename=$filename."_Affaire-".$_POST["affaire"].'_Date-' . date('d-m-Y-H-i-s') . '.xlsx';
	}
	else{
		$filename=$filename."_".$_POST["condition_text"].'_Date-' . date('d-m-Y-H-i-s') . '.xlsx';
		$result = $db->export_filtered(explode(";",$_POST["conditions"]));
	}

	if (isset($result) && isset($result[0])) {

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();
		$spreadsheet->addExternalSheet(clone $spreadsheet1->getSheetByName('Pivot'));
		$spreadsheet->addExternalSheet(clone $spreadsheet1->getSheetByName('Liste -'));
		$spreadsheet->getSheetByName('Pivot');


		// Set workbook properties
		$spreadsheet->getProperties()->setCreator('Actemium')
						->setLastModifiedBy('Actemium');

		$sheetIndex = $spreadsheet->getIndex($spreadsheet->getSheetByName('Worksheet'));
		$spreadsheet->removeSheetByIndex($sheetIndex);
		//////////////////////////// FILE INSERTION HERE ////////////////////////////
		// Start while loop to get data
		$i = 4;
		foreach ($result as $row) {
			$spreadsheet->getActiveSheet()->SetCellValue("A" . $i, $row["Parent"]); // Parent
			$spreadsheet->getActiveSheet()->SetCellValue("B" . $i, $row["Repere"]); // Repère
			$spreadsheet->getActiveSheet()->SetCellValue("E" . $i, $row["NumPlan"]); // Numéro de Plan
			$spreadsheet->getActiveSheet()->SetCellValue("F" . $i, $row["Indice"]); // Indice
			$spreadsheet->getActiveSheet()->SetCellValue("G" . $i, $row["Designation"]); // Désignation
			$spreadsheet->getActiveSheet()->SetCellValue("H" . $i, $row["QteU"]); // Quantité unitaire
			$spreadsheet->getActiveSheet()->SetCellValue("I" . $i, $row["QteT"]); // Quantité totale
			$spreadsheet->getActiveSheet()->SetCellValue("J" . $i, $row["EltType"]); // Type d'élément
			$spreadsheet->getActiveSheet()->SetCellValue("K" . $i, $row["Fab"]); // Fabrication
			$spreadsheet->getActiveSheet()->SetCellValue("L" . $i, $row["Traitements_Surface"]); // Traitement de surface
			$spreadsheet->getActiveSheet()->SetCellValue("V" . $i, $row["PrixU"]); // Prix unitaire
			$spreadsheet->getActiveSheet()->SetCellValue("X" . $i, $row["Delais"]); // Délais
			$spreadsheet->getActiveSheet()->SetCellValue("R" . $i, $row["Obs"]); // Observation

			$req2 = $db->select(array("fe.reference","f.nom"),array("fabricants_elements fe","fabricants f"),array("f.id=fe.id_fabricant","fe.id_element=".$row["Id"]),0);

			$refCol="M";
			$fabCol="N";
			for ($k = 0; $k < 2; ++$k) {
				if ($data = $req2->fetch()) {
					$spreadsheet->getActiveSheet()->SetCellValue($refCol . $i, $data[0]);
					$spreadsheet->getActiveSheet()->SetCellValue($fabCol . $i, $data[1]);
				}
				$refCol++;
				$refCol++;
				$fabCol++;
				$fabCol++;
			}
			++$i;
		}

		$writer = IOFactory::createWriter($spreadsheet, "Xlsx");

		$response = new StreamedResponse(
						function () use ($writer) {
							$writer->save('php://output');
						}
		);
		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
		$response->headers->set('Content-Disposition', 'attachment;filename="' . $filename . '"');
		$response->headers->set('Cache-Control', 'max-age=0');
		$response->send();
	}
	else
		header("Location:/traitement-pivot/view/accueil.php");
}
else
	header("Location:/traitement-pivot/view/accueil.php");