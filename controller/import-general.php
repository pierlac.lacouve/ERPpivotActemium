<?php

include "C:/wamp64/www/traitement-pivot/controller/auto-import.php";
require_once '../PHPSpreadSheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

if(isset($_POST)) {
	$data = $db->select(array("*"), array("affaires"), array("id='" . $_POST["affaire"] . "'"));
	$request=$db->select(array("*"),array("import_configuration"),array(1),0);
	for($i=1;$reqRes=$request->fetch();++$i){
		if(key_exists($reqRes["id"],$_POST)){
			$id=$reqRes["id"];
		}
	}
	$importData=$db->select(array("*"),array("import_configuration"),array("id=".$id));

	if (isset($data["login_chef_projet"])) {

		$length = 1;
		if($importData["import_mode"]==0)
			$length=sizeof($_FILES["file"]["name"]);
		$arrAll = array("*");
		$affaire = $_POST['affaire'];

		for ($fileCount = 0; $fileCount < $length; ++$fileCount) {
			switch ($importData["import_mode"]) {
				case 0:
					$inputfilname = $_FILES["file"]['tmp_name'][$fileCount];
					break;
				case 1:
					$inputfilname = $_FILES["file"]['tmp_name'];
					break;
			}
			try {
				$inputfiletype = IOFactory::identify($inputfilname);
				$objReader = IOFactory::createReader($inputfiletype);
				if(mb_strtolower($inputfiletype)==mb_strtolower('CSV'))
					$objReader->setInputEncoding('CP1252');
				$spreadsheet = $objReader->load($inputfilname);
			} catch (Exception $e) {
				die('Error loading file  "' . pathinfo($inputfilname, PATHINFO_BASENAME) . '": ' . $e->getMessage());
			}

			$sheet = $spreadsheet->getActiveSheet();
			$highestRow = $sheet->getHighestRow();

			if ($importData["import_mode"]==0) {
				$data = getPlanNumber(strtok($_FILES["file"]['name'][$fileCount], "."), 0);
				$parent = $data[0];
				$version = $data[1];

				//Verification de l'existance du sous ensemble parent
				$parentId = $db->getPartId($parent);
				if ($parentId != "") {
					$db->update("elements", array("indice"), array($version), array("id=" . $parentId));
				} else {
					$parentId = $db->insert("elements", array("numero_plan", "indice", "type"), array($parent, $version, "ens"), 1);
				}

				//Lier l'ensemble et l'affaire si il n'existe pas déjà
				if (!($db->select(array("id_parent"), array("hierarchies"), array("id_self=" . $parentId, "id_affaire='" . $affaire . "'")))) {
					$db->insert("hierarchies", array("id_parent", "id_self", "quantity", "id_affaire"), array("0", $parentId, "1", $affaire));
				}
			}

			for ($line = $importData["first_data_line"]; $line <= $highestRow; $line++) {

				$qte = 1;
				$rep = "NULL";
				$noP = "NULL";
				$ind="!";
				$des = "NULL";
				$ref = array();
				$frn = array();
				$type = "NULL";
				$fab = "NULL";
				$TrSur = "NULL";
				$obs = "NULL";
				if(isset($importData["quantity_col"]))
				$qte = getCellData($importData["quantity_col"] . $line);
				if(isset($importData["rep_col"]))
				$rep = getCellData($importData["rep_col"] . $line);
				if(isset($importData["planNum_col"]))
				$noP = getCellData($importData["planNum_col"] . $line);
				if(isset($importData["indice_col"]))
					$ind = $db->formatIndice(getCellData($importData["indice_col"] . $line));
				if(isset($importData["designation_col"]))
					$des = getCellData($importData["designation_col"] . $line);

				if(isset($importData["references_col"])&&isset($importData["fabricants_col"])) {
					$refCols = explode(";", $importData["references_col"]);
					$fabCols = explode(";", $importData["fabricants_col"]);
					for ($i = 0; $i < sizeof($refCols) && sizeof($refCols) == sizeof($fabCols); ++$i) {
						$ref[] = getCellData($refCols[$i] . $line);
						$frn[] = getCellData($fabCols[$i] . $line);
					}
				}

				if(isset($importData["type_col"]))
					$type = getCellData($importData["type_col"] . $line);
				if(isset($importData["fab_col"]))
					$fab = getCellData($importData["fab_col"] . $line);
				if(isset($importData["trSur_col"]))
					$TrSur = getCellData($importData["trSur_col"] . $line);
				if(isset($importData["level_col"]))
					$level = getCellData($importData["level_col"] . $line);
				if(isset($importData["ob_col"]))
					$obs = getCellData($importData["ob_col"] . $line);

				if($importData["import_mode"]==1) {
					$parentId = 0;
					for ($i = $line - 1; $level != 0 && $level <= intval(getCellData("C" . $i)); $i--)
						continue;
					if ($level != 0)
						$parentId = $db->getPartId(getCellData("D" . $i));
				}

				$db->insertPart($parentId, $qte, $rep, $noP, $ind, $des, $ref, $frn, $fab, $type,$TrSur, $obs, $affaire);
			}
			unset($inputfilname,$inputfiletype,$objReader,$spreadsheet,$sheet,$highestRow,$data,$parent,$version,$parentId);
		}
		if (!isset($_SESSION["erreur"]))
			$_SESSION["OK"] = "Import réussi";
		header("Location:../view/upload.php");
	} else {
		$_SESSION["affaire-send"]=$_POST["affaire"];
		header("Location:../view/affaires.php");
	}
}