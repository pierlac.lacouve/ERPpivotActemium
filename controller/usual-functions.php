<?php

	
	
	/** Gets the plan name and the version of any assembly and part
	 *
	 * @param string $fullName
	 * @param int $startCrop
	 * @return array
	 */
	function getPlanNumber($fullName,$startCrop)
	{
        $fullName=str_replace(array(" ","\r","\n","\t"),"",$fullName);
		$send=array();
		if (substr($fullName, -2, 1) == '-') {
			$send[0] = substr($fullName, $startCrop, -2);
			$send[1] = substr($fullName, -1, 1);
		} else {
			$send[0] = substr($fullName, $startCrop);
			$send[1] = "!";
		}
		return $send;
	}


	/** Gets cell data if not empty and format it for SQL queries
	 * @param $cell
	 * @return mixed|string
	 */
	function getCellData($cell){
		global $sheet;
		
		$data = str_replace(array("\r","\n","\t"),"",$sheet->getCell($cell, NULL, TRUE, FALSE));
		//$data = str_replace("'",'\'',$data);
		if ($data == "")
			$data="NULL";
		return($data);
	}


    /** Creates a random password size between 8 and 12
     *
     * @return string
     */

    function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = 61; //put the length -1 in cache
        $passLength = rand(8, 12);
        for ($i = 0; $i < $passLength; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function print_arr($a=array()){
    	foreach ($a as $val){
    		print("<p>".$val."</p>");
			}
		}

		function printTd($data){
			if(isset($data) && $data!="")
				print "<td>".$data."</td>";
			else
				print "<td> </td>";
		}

		function printTdEditable($level,$data,$id){
			print("<td><div ");
			if($level>0){
				print(" contentEditable='true' class='edit'");
			}
			print("id='" .$id. "'>");
			print($data);
			print("</div></td>");
		}


		function createPivotCondition($column,$postData=array())
		{
			$condition="1";

			$tempArr = array();
			if (isset($postData)) {
				foreach ($postData as $temp) {
					if ($temp != "")
						$tempArr[] = $column . "='" . $temp . "'";
				}
				if (isset($tempArr[0])) {
					$condition = "(" . implode(" OR ", $tempArr) . ")";
				}
			}
			return $condition;
		}

		function columnOptionList($maxCol,$unselectable=true,$defaultSelect="NULL",$multiple=false){
    	if($unselectable){
				print("<option value='NULL'");
				if($defaultSelect=="NULL")
					print(" selected");
				print(">Absent</option>");
			}

    	if($multiple)
    		$dataMultiple=explode(";",$defaultSelect);
    	else
				$dataMultiple=array($defaultSelect);


    	$cols="A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P/Q/R/S/T/U/V/W/X/Y/Z";
    	$arrCol=explode("/",$cols);
    	$i=0;
    	$j=-1;
    	do{
    		$col=$arrCol[$i];
    		if($j>=0)
    			$col=$arrCol[$j].$arrCol[$i];
    		$selected="";
    		if(in_array($col,$dataMultiple))
    			$selected=" selected";
    		print("<option value='".$col."'".$selected.">".$col."</option>");

    		++$i;
    		$i=$i%26;
    		if($i==0)
    			++$j;
			}while($col!=$maxCol);
		}
