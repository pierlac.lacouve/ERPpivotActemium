<?php
include_once ("auto-import.php");
if(isset($_POST["import-data"]) && $_POST["import-data"]!="Cancel") {
	if (sizeof($_POST["references_col"]) == sizeof($_POST["fabricants_col"])) {
		$target_Path = "../images/";
		$target_Path = $target_Path . basename($_FILES['file']['name']);
		if (!move_uploaded_file($_FILES['file']['tmp_name'], $target_Path))
			$target_Path = "NULL";
		$columnsName = array("nom",
						"import_mode",
						"color",
						"first_data_line",
						"path_icon",
						"quantity_col",
						"rep_col",
						"planNum_col",
						"indice_col",
						"designation_col",
						"references_col",
						"fabricants_col",
						"type_col",
						"fab_col",
						"trSur_col",
						"level_col",
						"ob_col");
		$import_mode = 0;
		if (isset($_POST["import_mode"]))
			$import_mode = 1;
		else
			$_POST["level_col"] = "NULL";
		$columnsValues = array($_POST["nom"],
						$import_mode,
						$_POST["color"],
						$_POST["first_data_line"],
						$target_Path,
						$_POST["quantity_col"],
						$_POST["rep_col"],
						$_POST["planNum_col"],
						$_POST["indice_col"],
						$_POST["designation_col"],
						implode(";", $_POST["references_col"]),
						implode(";", $_POST["fabricants_col"]),
						$_POST["type_col"],
						$_POST["fab_col"],
						$_POST["trSur_col"],
						$_POST["level_col"],
						$_POST["ob_col"]);

		$testArr = array($_POST["nom"],
						$import_mode,
						$_POST["color"],
						$_POST["first_data_line"],
						$target_Path,
						$_POST["quantity_col"],
						$_POST["rep_col"],
						$_POST["planNum_col"],
						$_POST["indice_col"],
						$_POST["designation_col"],
						$_POST["type_col"],
						$_POST["fab_col"],
						$_POST["trSur_col"],
						$_POST["level_col"],
						$_POST["ob_col"]);
		$testArr = array_merge($testArr, $_POST["references_col"], $_POST["fabricants_col"]);
		$testArr = array_diff($testArr, array("NULL"));

		if ($testArr == array_unique($testArr)) {
			if ($_POST["import-data"] == "new") {
				$db->insert("import_configuration", $columnsName, $columnsValues);
			} else {
				$db->update("import_configuration", $columnsName, $columnsValues, array("id=" . $_POST["import-data"]));
			}
		} else {
			$_SESSION["erreur"][] = "erreur : Vous ne pouvez pas attribuer une même colonne à deux champs de données différents !";
		}
	} else {
		$_SESSION["erreur"][] = "erreur : Vous n'avez pas attribué autant de colonnes de références que de fabriquants !";
	}
	if (isset($_SESSION["erreur"])) {
		$_POST["edit-import"]=$_POST["import-data"];
		$_SESSION["restart-import"] = $_POST;
	}
}
header("Location:../view/configurateur.php");