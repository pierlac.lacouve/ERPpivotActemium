<?php


class Stack
{
    private $dataArray;
    private $last_rank;

    public function __construct()
    {
        $this->dataArray=array();
        $this->last_rank=-1;
    }

    public function push($value){
        $this->dataArray[$this->last_rank+1]=$value;
        $this->last_rank=sizeof($this->dataArray)-1;
    }

    public function pop()
    {
        $val = false;
        if (!$this->isEmpty()) {
            $val = ($this->dataArray)[$this->last_rank];
            unset($this->dataArray[$this->last_rank]);
            $this->last_rank = sizeof($this->dataArray) - 1;
        }
        return $val;
    }

    public function isEmpty(){
        return $this->last_rank==-1;
    }

}