<?php
include_once "Stack.php";

class database
{
	private $pdo;


	public

		/** Constructor
		 */
	function __construct()
	{
		try {
			$strConnection = 'mysql:host=localhost;dbname=pivot;charset=utf8';
			$this->pdo = new PDO ($strConnection, 'pageAccess', 'lZ2zv2WrlhfiIOqa');
		} catch (PDOException $e) {
			session_start();
			$_SESSION['erreur'][] = 'ERROR PDO dans ' . $e->getMessage();
			header("location:../index.php");
		}
	}


	/** Get the part Id from either the plan number or the constructor reference
	 * @param $noP
	 * @param string $ref
	 * @param string $fab
	 * @return string
	 */
	function getPartId($noP, $ref = "NULL", $fab = "NULL",$des="NULL")
	{
		if ($noP != "NULL") {
			if ($data = $this->select(array("id", "numero_plan"), array("elements"), array("numero_plan='" . $noP . "'"))) {
				return intval($data["id"]);
			}
		} elseif ($des != "NULL") {
			if ($data = $this->select(array("id", "numero_plan", "designation"), array("elements"), array("designation='" . $des . "'"))) {
				return intval($data["id"]);
			}
		} else {
			if ($data = $this->select(array("id", "nom"), array("fabricants"), array("nom='" . $fab . "'"))) {
				if ($data = $this->select(array("id_element", "id_fabricant", "reference"), array("fabricants_elements"), array("id_fabricant='" . $data["id"] . "'", "reference='" . $ref . "'"))) {
					return intval($data["id_element"]);
				}
			}
		}
		return "";
	}

	/** Get the part Id from either the plan number or the constructor reference with multiple constructors
	 * @param string $noP
	 * @param array $ref
	 * @param array $fab
	 * @return string
	 */
	function getPartIdArray($noP, $ref, $fab,$des="NULL")
	{
		$ret = "";
		for ($i = 0; $i < sizeof($ref); ++$i) {
			$temp = $this->getPartId($noP, $ref[$i], $fab[$i],$des);
			if ($temp != "") {
				$ret = $temp;
			}
		}
		return $ret;
	}

	/** Sends a select sql query
	 * @param $columns
	 * @param $tables
	 * @param array $conditions
	 * @param int $fetch
	 * @param string $orderBy
	 * @return bool|mixed|PDOStatement
	 */
	function select($columns, $tables, $conditions = array(), $fetch = 1, $orderBy = "", $maxrow = -1)
	{
		$columnsStr = implode(",", $columns);
		$tablesStr = implode(",", $tables);
		if (!isset($conditions[0]))
			$conditions = array(1);
		if ($orderBy != "")
			$orderBy = " ORDER BY " . $orderBy;
		if ($maxrow != -1)
			$maxrow = " LIMIT " . $maxrow;
		else
			$maxrow = "";
		$conditionsStr = " WHERE " . implode(" AND ", $conditions) . $orderBy . $maxrow;

		$req = $this->pdo->prepare("SELECT DISTINCT " . $columnsStr . " FROM " . $tablesStr . $conditionsStr . ";");
		//print("<p>SELECT " . $columnsStr . " FROM " . $tablesStr . $conditionsStr . ";</p>");
		$req->execute();

		if ($fetch == 1)
			return $req->fetch();
		else
			return $req;
	}

	/**
	 * @param $table
	 * @param $columns
	 * @param $valuesArr
	 * @param int $returnId
	 * @return string
	 */
	function insert($table, $columns, $valuesArr, $returnId = 0)
	{
		$values = $this->format($valuesArr);
		$columnsStr = implode(",", $columns);
		$valuesStr = implode(",", $values);
		$reqStr = "INSERT INTO " . $table . " (" . $columnsStr . ") VALUES (" . $valuesStr . ");";

		$stmt = $this->pdo->prepare($reqStr);
		if (!$stmt->execute()) {
			$_SESSION["erreur"][] = "erreur : " . $stmt->errorInfo()[2] . " : " . $reqStr;
		}
		if ($returnId == 1) {
			$lii = $this->pdo->lastInsertId();
			return $lii;
		}
	}

	/** Updates database line
	 * @param $table
	 * @param $columns
	 * @param $valuesArr
	 * @param array $conditions
	 */
	function update($table, $columns, $valuesArr, $conditions = array(),$setNull=false)
	{
		$values = $this->format($valuesArr);
		$reqStr = "UPDATE " . $table . " SET ";
		$update = array();
		for ($i = 0; $i < sizeof($columns); ++$i) {
			if ($values[$i] != "NULL" || $setNull)
				$update[] = $columns[$i] . "=" . $values[$i];
		}

		if (!isset($conditions[0]))
			$conditions = array(1);
		$reqStr = $reqStr . " " . implode(", ", $update) . " WHERE " . implode(" AND ", $conditions) . ";";
		$stmt = $this->pdo->prepare($reqStr);
		if (!$stmt->execute()) {
			$_SESSION["erreur"][] = "erreur : " . $stmt->errorInfo()[2] . " : " . $reqStr;
		}
	}

	/** Deletes lines from a specified table respecting the conditions
	 * @param $table
	 * @param array $conditions
	 */
	function delete($table, $conditions = array())
	{
		$reqStr = "DELETE FROM " . $table . " ";

		if (!isset($conditions[0]))
			$conditions = array(1);
		$reqStr = $reqStr . " WHERE " . implode(" AND ", $conditions) . ";";
		$stmt = $this->pdo->prepare($reqStr);
		if (!$stmt->execute()) {
			$_SESSION["erreur"][] = "erreur : " . $stmt->errorInfo()[2] . " : " . $reqStr;
		}
	}

	/** Adds a character at the extremities of all strings from an array (default is "'")
	 * @param array $data
	 * @param string $char
	 * @return array
	 */
	function format($data, $char = "'")
	{
		$dataFormat = array();
		foreach ($data as $str) {
			if ($str == "NULL")
				array_push($dataFormat, $str);
			else
				array_push($dataFormat, $char . str_replace("'", "\'", $str) . $char);
		}
		return $dataFormat;
	}


	/** Exports the data of Pivot database with specified conditions and counting total number of parts needed to build
	 *  "Parent Racine" called by it's database ID
	 *
	 * @param array $conditions
	 * @param $IDParentRacine
	 * @return array
	 */
	function export_pivot($conditions = array(), $IDParentRacine = 0)
	{
	    $this->cleanup_database();
		return $this->export_pivot_derec($conditions, array($IDParentRacine));
	}

    /**
     * @param $conditions
     * @param $arrayAncestors
     * @return array
     */
    function export_pivot_derec($conditions, $arrayAncestors)
    {
        $stack=new Stack();
        $res = array();
        $stack->push(array("conditions"=>$conditions,"arrayAncestors"=>$arrayAncestors));
        while(!$stack->isEmpty()){
            $env_vars=$stack->pop();
            $arrayAncestors=$env_vars["arrayAncestors"];
            if(isset($env_vars["row"])){
                $res[]=$env_vars["row"];
            }
            $conditionsT = $env_vars["conditions"];
            $conditionsT[] = "h.id_parent='" . end($env_vars["arrayAncestors"]) . "'";

            $req = $this->pdo->prepare("SELECT DISTINCT h.id_affaire Affaire, e2.numero_plan Parent, h.repere Repere, e1.numero_plan NumPlan, e1.indice Indice,e1.designation Designation, h.quantity QteU,e1.type EltType,e1.fabrication Fab, e1.traitement_surface Traitements_Surface,fe.prix PrixU, fe.delais Delais,h.observation AS Obs, e1.id Id, e2.id IdParent, h.statut Statut 
		FROM ((elements e1 JOIN hierarchies h ON e1.id=h.id_self) JOIN elements e2 ON h.id_parent=e2.id) LEFT JOIN (fabricants_elements fe JOIN fabricants f ON f.id=fe.id_fabricant) ON e1.id=fe.id_element
		WHERE " . implode(" AND ", $conditionsT) . " ORDER BY Parent,NumPlan DESC;");
            $req->execute();

            while ($row = $req->fetch()) {
                //print("<p>".$row["Id"]."</p>");
                $row["QteT"] = $this->calculateTotalQuantity($row['Id'], $conditions, $arrayAncestors);
                $arrayAncestorsTemp = $arrayAncestors;
                $arrayAncestorsTemp[] = $row["Id"];

                $stack->push(array("row"=>$row,"conditions"=>$conditions,"arrayAncestors"=>$arrayAncestorsTemp));
            }
        }
        return ($res);
    }

    function cleanup_database(){
        $request=$this->select(array("id_parent","id_self","id_affaire"),array("hierarchies"),array("id_parent='0'"),0);
        $stack=new Stack();
        while($temp=$request->fetch()){
            //check for loop in temp
            $arrayAncestors=array("0");

            $stack->push(array("row"=>$temp,"arrayAncestors"=>$arrayAncestors));
            while(!$stack->isEmpty()){
                $env_vars=$stack->pop();

                $arrayAncestors=$env_vars["arrayAncestors"];
                $conditionsT = array("id_affaire='".$env_vars["row"]["id_affaire"]."'");
                $conditionsT[] = "id_parent='" . end($env_vars["arrayAncestors"]) . "'";

                $req = $this->select(array("*"),array("hierarchies"), $conditionsT,0, "id_self ASC");
                while ($row = $req->fetch()) {
                    //print("<p>".$row["id_self"]."</p>");
                    if(in_array($row["id_self"],$arrayAncestors)) {
                        $piece = $this->select(array("*"), array("elements"), array("id='" . $row["id_self"] . "'"));
                        $parent = $this->select(array("*"), array("elements"), array("id='" . $row["id_parent"] . "'"));
                        $affaire = $this->select(array("*"), array("affaires"), array("id='" . $row["id_affaire"] . "'"));

                        $error = "<p>ATTENTION DES DONNEES ERRONNEES ONT ETE SUPPRIMEES VERIFIEZ LES DERNIERES MODIFICATIONS !!!</p>";
                        $error = $error . "<pre>Données Pièce : " . print_r(array(
                                "id_database" => $piece["id"],
                                "nop" => $piece["numero_plan"],
                                "ind" => $piece["indice"],
                                "designation" => $piece["designation"],
                                "type" => $piece["type"],
                                "fabrication" => $piece["fabrication"],
                                "tr_sur" => $piece["traitement_surface"]
                            ),true)."</pre>";

                        $error = $error . "<pre>Données Parent : " . print_r(array(
                                "id_database" => $parent["id"],
                                "nop" => $parent["numero_plan"],
                                "ind" => $parent["indice"],
                                "designation" => $parent["designation"],
                                "type" => $parent["type"],
                                "fabrication" => $parent["fabrication"],
                                "tr_sur" => $parent["traitement_surface"]
                            ),true)."</pre>";

                        $error = $error . "<pre>Données Affaire : " . print_r(array(
                                "id_database" => $affaire["id"],
                                "libelle" => $affaire["designation"],
                                "chef_projet" => $affaire["login_chef_projet"]
                            ),true)."</pre>";

                        $_SESSION["erreur"][] = $error;
                        $this->delete("hierarchies", array("id_parent='" . end($env_vars["arrayAncestors"]) . "'", "id_self='" . $row["id_self"] . "'", "id_affaire='" . $row["id_affaire"] . "'"));
                    }else {
                        $arrayAncestorsTemp = $arrayAncestors;
                        $arrayAncestorsTemp[] = $row["id_self"];

                        $stack->push(array("row" => $row, "arrayAncestors" => $arrayAncestorsTemp));
                    }
                }
            }
        }
    }

	function export_filtered($conditions = array(1))
	{
        $this->cleanup_database();
		$res = array();
		$req = $this->pdo->prepare("SELECT DISTINCT h.id_affaire Affaire, e2.numero_plan Parent, h.repere Repere, e1.numero_plan NumPlan, e1.indice Indice,e1.designation Designation, h.quantity QteU,e1.type EltType,e1.fabrication Fab, e1.traitement_surface Traitements_Surface,fe.prix PrixU, fe.delais Delais,h.observation Obs, e1.id Id, e2.id IdParent, h.statut Statut 
		FROM ((elements e1 JOIN hierarchies h ON e1.id=h.id_self) JOIN elements e2 ON h.id_parent=e2.id) LEFT JOIN (fabricants_elements fe JOIN fabricants f ON f.id=fe.id_fabricant) ON e1.id=fe.id_element
		WHERE " . implode(" AND ", $conditions) . " ORDER BY Parent,NumPlan ASC;");
		$req->execute();

		while ($row = $req->fetch()) {
			$row["QteT"] = "/";
			$res[] = $row;
		}
		return ($res);
	}

	/** Calculates recursively the total number of a part or an assembly needed in the project
	 * @param $idElt
	 * @param $conditions
	 * @param array int $arrayAncestors
	 * @return int
	 */
	function calculateTotalQuantity($idElt, $conditions, $arrayAncestors = array(0))
	{
		$result = 1;
		$arrayAncestors[] = $idElt;

		for ($i = sizeof($arrayAncestors) - 1; $i > 0; --$i) {
			$ancestor = $arrayAncestors[$i - 1];
			$son = $arrayAncestors[$i];

			$conditionsT = array_merge(array("h.id_self='" . $son . "'", "h.id_parent='" . $ancestor . "'"), $conditions);
			$res = $this->select(array("*"), array("hierarchies h"), $conditionsT);
			$result *= $res["quantity"];
		}
		return $result;
	}

	/**
	 *
	 */
	function reinitialize()
	{
		$this->pdo->prepare(file_get_contents("C:/wamp64/www/traitement-pivot/assets/pivot.sql"))->execute();
	}


	/**
	 * @param $parentId
	 * @param $qte
	 * @param $rep
	 * @param string $noP
	 * @param string $ind
	 * @param string $des
	 * @param string array $refA
	 * @param string array $frnA
	 * @param $fab
	 * @param $type
	 * @param $Obs
	 * @param $affaire
	 */
	function insertPart($parentId, $qte, $rep, $noP, $ind, $des, $refA, $frnA, $fab, $type, $TrSur,$Obs, $affaire)
	{
		if (!($noP == "NULL" && $ind == "!" && $des == "NULL")) {
			if($type!="NULL")
				$type = strtolower(str_replace(" ", "", $type));
			//elements : Ajout de l'ensemble si il n'existe pas déjà, sinon, on le met à jour. OK

			$pieceId = $this->getPartIdArray($noP, $refA, $frnA);
			if ($pieceId != "") {
				$this->update("elements", array("indice", "fabrication", "designation","traitement_surface","type"), array($ind, $fab, $des,$TrSur,$type), array("id=" . $pieceId . ""));
			} else {
				$pieceId = $this->insert("elements", array("numero_plan", "indice", "fabrication", "designation","traitement_surface", "type"), array($noP, $ind, $fab, $des,$TrSur, $type), 1);
			}

			//HIERARCHIE DES AFFAIRES : Ajout de l'ensemble dans l'affaire si l'asociation n'existe pas déjà. OK
			if (!($this->select(array("id_self", "id_affaire", "id_parent"), array("hierarchies"), array("id_self='" . $pieceId . "'", "id_affaire='" . $affaire . "'", "id_parent='0'")))) {
                if (!($this->select(array("id_self", "id_affaire", "id_parent"), array("hierarchies"), array("id_self='" . $pieceId . "'", "id_affaire='" . $affaire . "'", "id_parent='" . $parentId . "'")))) {
                    if ($parentId != $pieceId && intval($pieceId) != 0)
                        $this->insert("hierarchies", array("id_parent", "id_self", "quantity", "id_affaire", "repere"), array($parentId, $pieceId, $qte, $affaire, $rep));
                    else
                        $_SESSION["erreur"][] = "erreur : Référencement de la pièce suivante invalide : " . $noP . "; " . $des . "; " . $refA[0];
                    //print("<p>added $noP</p>");
                } else {
                    $this->update("hierarchies", array("quantity", "repere", "observation"), array($qte, $rep, $Obs), array("id_self='" . $pieceId . "'", "id_affaire='" . $affaire . "'", "id_parent='" . $parentId . "'"));
                    //print("<p>updated $noP no '0'</p>");
                }
            } else {
				$this->update("hierarchies", array("quantity", "id_parent", "repere","observation"), array($qte, $parentId, $rep,$Obs), array("id_self='" . $pieceId . "'", "id_affaire='" . $affaire . "'", "id_parent='0'"));
				//print("<p>updated $noP w/ '0'</p>");
			}

			for ($i = 0; $i < sizeof($refA) && sizeof($refA) == sizeof($frnA); $i++) {
				$frn = $frnA[$i];
				$ref = $refA[$i];
				if ($frn != 'NULL') {
					if (!($data3 = $this->select(array("*"), array("fabricants"), array("nom='" . $frn . "'"))))
						$fId = $this->insert("fabricants", array("nom"), array($frn), 1);
					else
						$fId = $data3["id"];
					if (!($this->select(array("*"), array("fabricants_elements"), array("id_element='" . $pieceId . "'", "id_fabricant='" . $fId . "'")))) {
						$this->insert("fabricants_elements", array("id_element", "id_fabricant", "quantity", "reference"), array($pieceId, $fId, $qte, $ref));
					} else {
						$this->update("fabricants_elements", array("quantity", "reference"), array($qte, $ref), array("id_element='" . $pieceId . "'", "id_fabricant='" . $fId . "'"));
					}
				}
			}
		}
	}

	/**
	 * @param string $data
	 * @return string
	 */
	function formatIndice($data = "NULL")
	{
		if ($data != "NULL")
			$data = strtolower(str_replace(array(" ", "\n", "\t"), "", $data));
		if ($data == "" || $data == "NULL")
			$data = "!";
		return $data;
	}

	/**
	 * @param bool $isDataLoaded
	 * @param bool $doColor
	 */
	function getAffaires($isDataLoaded = false, $doColor = false)
	{
		if (!$isDataLoaded)
			$tableSelect = "affaires a";
		else
			$tableSelect = "affaires a join hierarchies h on a.id=h.id_affaire";

		$pf = $this->select(array("a.id", "a.designation", "a.login_chef_projet"), array($tableSelect), array(), 0);
		while ($Qt = $pf->fetch()) {
			$color = "green";
			if (!$isDataLoaded) {
				if ($Qt["login_chef_projet"] == "")
					$color = "red";
			}
			print("<option value='" . $Qt['id'] . "'");
			if ($doColor)
				print("style='color:" . $color . "'");
			print(">" . $Qt['id'] . " - " . $Qt['designation'] . "</option>");
		}
		$pf->closeCursor();
	}

	/**
	 * @param $tableName
	 * @param $idCol
	 * @param $nameCol
	 * @return array|int
	 */
	public function getTable($tableName, $idCol, $nameCol = "NULL",$condition=1)
	{
		$strReq = "SELECT DISTINCT " . $idCol . " as ID, \"\" as NAME from " . $tableName . " WHERE ".$condition.";";
		if ($nameCol != "")
			$strReq = "SELECT DISTINCT " . $idCol . " as ID, " . $nameCol . " as NAME from " . $tableName . " WHERE ".$condition.";";
		$req = $this->pdo->prepare($strReq);
		$req->execute();
		return $req;
	}
}