<?php
$pagesPaths=["Administration"=>"/traitement-pivot/view/management.php",
				"AccountsManager"=>"/traitement-pivot/view/accounts-manager.php",
				"Accueil"=>"/traitement-pivot/view/accueil.php",
				"Affaires"=>"/traitement-pivot/view/affaires.php",
				"ChangePass"=>"/traitement-pivot/view/change-password.php",
				"Codex"=>"/traitement-pivot/view/codex.php",
				"Configurateur"=>"/traitement-pivot/view/configurateur.php",
				"MyAccount"=>"/traitement-pivot/view/my-account.php",
				"NewAccount"=>"/traitement-pivot/view/new-account.php",
				"Pivot"=>"/traitement-pivot/view/pivot.php",
				"Redirect"=>"/traitement-pivot/view/redirection.php",
				"Upload"=>"/traitement-pivot/view/upload.php",
				"modify-password" => "/traitement-pivot/controller/modify-password.php",
				"reinitialize" => "/traitement-pivot/controller/reinitialize.php",
				"insert-affaire" => "/traitement-pivot/controller/insert-affaire.php",
				"insert-account" => "/traitement-pivot/controller/insert-account.php",
				"reload-affaire" => "/traitement-pivot/controller/reload-affaire.php",
				"insert-client" => "/traitement-pivot/controller/insert-client.php",
				"logout" => "/traitement-pivot/controller/logout.php",
				"security" => "/traitement-pivot/controller/security.php",
				"import-general" => "/traitement-pivot/controller/import-general.php",
				"usual-functions" => "/traitement-pivot/controller/usual-functions.php",
				"export-pivot" => "/traitement-pivot/controller/export-pivot.php",
				"export-codex" => "/traitement-pivot/controller/export-codex.php",
				"database-connection" => "/traitement-pivot/controller/database-connection.php",
				"auto-import" => "/traitement-pivot/controller/auto-import.php",
				"authentication" => "/traitement-pivot/controller/authentication.php",
				"delete-single-row" => "/traitement-pivot/controller/delete-single-row.php",
				"delete-multiple-rows" => "/traitement-pivot/controller/delete-multiple-rows.php"];

//Header data

$pageLogo=["Accueil"=>"home","Pivot"=>"book","Codex"=>"shopping-cart","Affaires"=>"copy","Upload"=>"upload","Administration"=>"pencil-alt"];


class AuthorizationLevel
{
	private $_level;
	private $_HeaderPages;
	private $_AuthorizedPages;

	public
	/**
	 * Constructeur de la classe
	 *
	 * @param void
	 * @return void
	 */
	function __construct($level=0)
	{
		$this->setLevel($level);
	}

	/**
	 * @return int
	 */
	function getLevel()
	{
		return $this->_level;
	}

	/**
	 * @param int $level
	 */
	function setLevel($level)
	{
		$this->_level = $level;
		switch ($this->_level) {
			case 0:
				$this->_HeaderPages = ["Accueil", "Affaires", "Pivot"];
				$this->_AuthorizedPages = ["Accueil", "Affaires", "ChangePass", "MyAccount", "Pivot",
								"Redirect",
								"modify-password",
								"reload-affaire",
								"logout",
								"security",
								"usual-functions",
								"database-connection",
								"auto-import",
								"authentication"];
				break;
			case 1:
				$this->_HeaderPages = ["Accueil", "Affaires", "Upload", "Pivot", "Codex"];
				$this->_AuthorizedPages = ["Accueil", "Affaires", "ChangePass", "Codex", "MyAccount", "Pivot", "Upload",
								"Redirect",
								"modify-password",
								"insert-affaire",
								"reload-affaire",
								"insert-client",
								"logout",
								"security",
								"import-general",
								"usual-functions",
								"export-pivot",
								"export-codex",
								"database-connection",
								"auto-import",
								"authentication",
								"delete-multiple-rows",
								"delete-single-row"];
				break;
			case 2:
				$this->_HeaderPages = ["Accueil", "Affaires", "Upload", "Pivot", "Codex", "Administration"];
				break;
		}

	}

	/**
	 * @return array
	 */
	function getHeaderPages()
	{
		return $this->_HeaderPages;
	}

	/**
	 * @param $pagePath
	 * @param $pageName
	 * @param $pagesPaths
	 * @return bool
	 */
	function isSelectedHeaderPage($pagePath,$pageName,$pagesPaths){
		$res=$pagePath==$pagesPaths[$pageName];
		if(($pageName=="Administration"))
			$res=($pagePath==$pagesPaths["Configurateur"] || $pagePath==$pagesPaths["AccountsManager"] || $pagePath==$pagesPaths["NewAccount"] || $pagePath==$pagesPaths["Administration"] );
		return ($res);
	}

	/**
	 * @return bool
	 */
	function isAuthorized()
	{
		global $pagesPaths, $_SESSION;

		if (!(isset($_SESSION['firstname']))) {
			return false;
		} elseif ($_SESSION['first_connection'] == 1 && $_SERVER['PHP_SELF'] != "/traitement-pivot/view/change-password.php" && $_SERVER['PHP_SELF'] != "/traitement-pivot/controller/modify-password.php") {
			return false;
		}

		if($this->_level==2)
			return true;
		foreach ($this->_AuthorizedPages as $page) {
			if ($_SERVER['PHP_SELF'] == $pagesPaths[$page])
				return true;
		}
		return false;
	}

	/**
	 * @return string
	 */
	function redirectTo()
	{
		global $_SESSION;

		if ($_SESSION['first_connection'] == 1 && $_SERVER['PHP_SELF'] != "/traitement-pivot/view/change-password.php" && $_SERVER['PHP_SELF'] != "/traitement-pivot/controller/modify-password.php") {
			return ("Location:/traitement-pivot/view/change-password.php");
		}
		return ("Location:/traitement-pivot/view/accueil.php");
	}
}